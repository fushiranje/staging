<?php
/**
 * Copyright © 2017-2020 Dragan Đurić. All rights reserved.
 *
 * @package warp-imagick
 * @license GNU General Public License Version 2.
 * @copyright © 2017-2020 Dragan Đurić. All rights reserved.
 * @author Dragan Đurić <dragan dot djuritj at gmail dot com>
 * @link https://wordpress.org/plugins/warp-imagick/
 *
 * This copyright notice, source files, licenses and other included
 * materials are protected by U.S. and international copyright laws.
 * You are not allowed to remove or modify this or any other
 * copyright notice contained within this software package.
 */

namespace ddur\Warp_iMagick;

defined( 'ABSPATH' ) || die( -1 );

use \ddur\Warp_iMagick\Base\Plugin\v1\Lib;
use \ddur\Warp_iMagick\Base\Meta_Plugin;

if ( ! class_exists( __NAMESPACE__ . '\Plugin' ) ) {

	/** Plugin class */
	final class Plugin extends Meta_Plugin {

		// phpcs:ignore
	# region Properties

		/** Plugin Disabled
		 *
		 * Is plugin disabled due failed requirements?
		 *
		 * @var bool|array $disabled false or array of strings - requirements failed.
		 */
		private $disabled = false;

		/** File Path.
		 *
		 * Attachment file path.
		 *
		 * @var string $file_path stored.
		 */
		private $file_path = '';

		/** Orig Path.
		 *
		 * @since WP 5.3
		 *
		 * Original file path added in WP 5.3+
		 *
		 * @var string $orig_path stored.
		 */
		private $orig_path = '';

		/** Files Dir.
		 *
		 * Stores files directory.
		 *
		 * @var string $sizes_dir stored.
		 */
		private $sizes_dir = '';

		/** Mime Type.
		 *
		 * Stores mime type.
		 *
		 * @var string $mime_type stored.
		 */
		private $mime_type = '';

		/** Transparency.
		 *
		 * Stores transparency.
		 *
		 * @var null|bool $transparency stored.
		 */
		private $transparency = null;

		/** Can Generate WebP.
		 *
		 * @var bool $can_generate_webp property.
		 */
		private $can_generate_webp = null;

		/** Sizes stored
		 *
		 * Sizes removed and stored here at
		 * intermediate_image_sizes_advanced and
		 * Sizes restored from here at
		 * wp_generate_attachment_metadata.
		 *
		 * Value is false or array of stored sizes.
		 *
		 * @var bool|array $sizes stored.
		 */
		private $sizes = false;

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Plugin Class Initialization.

		/** Plugin init. Called immediately after plugin class is constructed. */
		protected function init() {

			\add_action( 'init', array( $this, 'handle_wordpress_init' ) );

			if ( is_admin() ) {

				require __DIR__ . '/class-settings.php';
				$this->load_textdomain();
				Settings::once( $this );
			}

		}

		/** WordPress Init */
		public function handle_wordpress_init() {

			$this->set_disabled( get_option( $this->get_option_id() . '_disabled' ) );

			\add_action( 'admin_notices', array( $this, 'handle_admin_notices' ) );

			/** User has access to Upload/Media menu? */
			if ( function_exists( '\get_current_user' )
			&& function_exists( '\current_user_can' )
			&& \current_user_can( 'upload_files' ) ) {
				if ( ! $this->is_disabled() ) {
					$this->add_template_endpoint();
					Lib::auto_hook( $this );
				}
			}
		}

		/** On admin notices action */
		public function handle_admin_notices() {

			if ( $this->is_disabled() ) {
				$counter = 0;
				$reasons = $this->why_is_disabled();
				foreach ( $reasons as $message ) {
					if ( is_string( $message ) && trim( $message ) ) {
						self::error( $message );
						$counter++;
					}
				}
				$plugin_name = $this->get_slug();
				$s_in_plural = 1 < $counter ? 's' : '';
				if ( 0 === $counter ) {
					self::error( "Plugin '$plugin_name': is DISABLED, no reason given." );
				} else {
					self::error( "Plugin '$plugin_name' is DISABLED due to activation failure$s_in_plural given above." );
				}
				self::error( "Please deactivate '$plugin_name' plugin and activate again when your site meets missing requirement$s_in_plural." );
			}

			$update_settings = \get_transient( $this->get_slug() . '-update-settings' );
			if ( is_array( $update_settings ) ) {
				foreach ( $update_settings as $type => $message ) {
					switch ( $type ) {
						case 'error':
							self::error( $message );
							break;
						default:
							self::admin_notice( $message );
							break;
					}
				}
			}
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Image Upload & Regenerate Hooks

		/** On 'big_image_size_threshold' filter
		 *
		 * Priority is high/late to override other filters.
		 *
		 * @param int $threshold is value in pixels. Default 2560.
		 */
		public function on_big_image_size_threshold_99_filter( $threshold ) {

			if ( $threshold && true === $this->get_option( 'wp-big-image-size-treshold-disabled' ) ) {
				$threshold = 0;
			}
			return $threshold;
		}

		/** On 'wp_image_editors' filter.
		 *
		 * Prepend derived editor to editors list.
		 *
		 * @param array $editors - image editors.
		 */
		public function on_wp_image_editors_filter( $editors ) {

			require_once __DIR__ . '/class-warp-image-editor-imagick.php';

			return array_merge( array( 'Warp_Image_Editor_Imagick' ), $editors );
		}

		/** On intermediate_image_sizes_advanced filter.
		 *
		 * Early priority (-100) will ignore RT plugin "skipping sizes".
		 *
		 * @param array $sizes - attachment sizes.
		 * @param array $metadata - attachment meta data.
		 */
		public function on_intermediate_image_sizes_advanced_minus100_filter( $sizes, $metadata = false ) {

			$this->sizes = false;

			if ( ! is_array( $sizes ) ) {
				return $sizes;
			}
			if ( ! self::is_valid_metadata( $metadata ) ) {
				return $sizes;
			}

			$image_mime_type = wp_check_filetype( $metadata ['file'] );
			$image_mime_type = $image_mime_type['type'];
			switch ( $image_mime_type ) {
				case 'image/jpeg':
				case 'image/png':
					$this->mime_type = $image_mime_type;
					$this->sizes     = $sizes;
					$sizes           = array();
					break;
				default:
					$this->sizes = false;
					break;
			}
			return $sizes;
		}

		/** On wp_generate_attachment_metadata filter.
		 *
		 * Replace wp_generate_attachment_metadata() functionality for JPEG/PNG images between
		 * intermediate_image_sizes_advanced and wp_generate_attachment_metadata hooks
		 * Late priority (+100) will overwrite RT plugin returned sizes.
		 *
		 * @param array $metadata - attachment meta data.
		 * @param int   $attachment_id - number.
		 */
		public function on_wp_generate_attachment_metadata_100_filter( $metadata, $attachment_id = false ) {

			if ( ! $this->sizes ) {
				return $metadata;
			}

			if ( ! self::is_valid_metadata( $metadata ) ) {
				return $metadata;
			}

			if ( ! array_key_exists( 'sizes', $metadata ) || ! is_array( $metadata ['sizes'] ) ) {
				$metadata ['sizes'] = array();
			}

			if ( ! array_key_exists( 'image_meta', $metadata ) || ! is_array( $metadata ['image_meta'] ) ) {
				$metadata ['image_meta'] = array();
			}

			$max_image_width = false;
			$is_resized      = false;

			$this->file_path = \trailingslashit( \wp_upload_dir() ['basedir'] ) . $metadata ['file'];
			$this->sizes_dir = \trailingslashit( dirname( $this->file_path ) );

			if ( \function_exists( '\\wp_get_original_image_path' ) && ! empty( $metadata ['original_image'] ) ) {

				$this->orig_path = \wp_get_original_image_path( $attachment_id );
			}
			$edit_path = $this->orig_path && file_exists( $this->orig_path ) ? $this->orig_path : $this->file_path;

			if ( 'image/png' === $this->mime_type ) {

				$this->transparency = $this->can_generate_webp_clones() ? $this->get_image_file_transparency( $edit_path, $this->mime_type ) : true;
			} else {
				$this->transparency = false;
			}

			if ( is_array( $this->sizes ) ) {

				if ( ! self::is_edited( $this->file_path ) ) {

					$max_image_width = $this->get_max_image_width();

					if ( $max_image_width && empty( $this->orig_path ) ) {

						$geometry_resized = $this->check_resize_image_width( $this->file_path, $this->file_path, $max_image_width );

						if ( $geometry_resized ) {
							$is_resized          = true;
							$metadata ['width']  = $geometry_resized ['width'];
							$metadata ['height'] = $geometry_resized ['height'];
						}
					}

					if ( $this->do_generate_webp_clones() && false === $this->transparency ) {

						$this->webp_clone_image( $this->file_path, $this->mime_type, $this->transparency );
						if ( $this->orig_path ) {

							$this->webp_clone_image( $this->orig_path, $this->mime_type, $this->transparency );
						}
					}
				}

				$editor = wp_get_image_editor( $edit_path );

				if ( is_wp_error( $editor ) ) {
					$metadata ['sizes'] = array();
					Lib::error( 'Function wp_get_image_editor() returned an error: ' . $editor->get_error_message() );
					return $metadata;
				}

				if ( 'Warp_Image_Editor_Imagick' !== get_class( $editor ) ) {
					Lib::error( 'Wrong editor class?: ' . get_class( $editor ) );
					$metadata ['sizes'] = array();
					return $metadata;
				}

				if ( \function_exists( '\\wp_create_image_subsizes' ) ) {

					if ( ! empty( $metadata['image_meta'] ) ) {
						$rotated = $editor->maybe_exif_rotate();

						if ( is_wp_error( $rotated ) ) {
							Lib::error( '$editor->maybe_exif_rotate() failed.' );
						}
					}

					if ( method_exists( $editor, 'make_subsize' ) ) {
						foreach ( $this->sizes as $new_size_name => $new_size_data ) {
							$new_size_meta = $editor->make_subsize( $new_size_data );

							if ( is_wp_error( $new_size_meta ) ) {
								Lib::debug( '$editor->make_subsize() failed for size name: ' . $new_size_name );
							} else {

								$metadata['sizes'][ $new_size_name ] = $new_size_meta;
								wp_update_attachment_metadata( $attachment_id, $metadata );
							}
						}
					} else {

						$created_sizes = $editor->multi_resize( $new_sizes );

						if ( ! empty( $created_sizes ) ) {
							$metadata['sizes'] = array_merge( $metadata['sizes'], $created_sizes );
							wp_update_attachment_metadata( $attachment_id, $metadata );
						}
					}
				} else {

					$metadata['sizes'] = $editor->multi_resize( $this->sizes );
					wp_update_attachment_metadata( $attachment_id, $metadata );
				}
			}

			return $metadata;
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Delete Post/Attachment Hooks

		/** On 'delete_attachment' action.
		 *
		 * Make sure webp files are deleted when attachment deleted.
		 *
		 * @param int $post_id - attachment id.
		 */
		public function on_delete_attachment_action( $post_id ) {

			add_filter(
				'wp_delete_file',
				function( $path ) {
					if ( trim( $path ) && is_readable( $path ) ) {
						$mime_type = wp_check_filetype( $path );
						$mime_type = $mime_type['type'];
						switch ( $mime_type ) {
							case 'image/jpeg':
							case 'image/png':
								$delete_webp = self::get_webp_file_name( $path );
								if ( file_exists( $delete_webp ) ) {
									unlink( $delete_webp );
								}
								break;
						}
					} else {
						Lib::debug( 'wp_delete_file filter, file not found or not readable: ' . wp_basename( $path ) );
					}
					return $path;
				}
			);
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Helper functions

		/** Set disabled property.
		 *
		 * @param bool|array $disabled to set.
		 */
		public function set_disabled( $disabled ) {

			if ( false === $disabled ) {
				$this->disabled = $disabled;
			} elseif ( is_array( $disabled ) ) {
				$this->disabled = $disabled;
			} elseif ( true === $disabled ) {
				$this->disabled = array( 'Disabled by no reason given.' );
			} else {
				$this->disabled = array( 'Disabled by unsupported argument value type.' );
			}
		}

		/** Plugin is disabled due activation failures (missing requirements). */
		public function is_disabled() {

			return false !== $this->disabled;

		}

		/** Returns array of strings (messages): activation failures (missing requirements). */
		public function why_is_disabled() {

			return false !== $this->disabled && is_array( $this->disabled ) ? $this->disabled : array();

		}

		/** Return max image width (if enabled by option) */
		public function get_max_image_width() {
			if ( $this->get_option( 'image-max-width-enabled', false ) ) {
				$max_image_width = $this->get_option( 'image-max-width-pixels', 0 );

				if ( $max_image_width >= self::max_width_min_val() && $max_image_width <= self::max_width_max_val() ) {
					return $max_image_width;
				}
			}
			return false;
		}

		/** Generate webp clones? */
		public function do_generate_webp_clones() {
			return $this->can_generate_webp_clones()
			&& $this->get_option( 'webp-images-create', false );
		}

		/** Can Generate webp clones? */
		public function can_generate_webp_clones() {
			if ( null === $this->can_generate_webp ) {
				$functions = array(
					'\\imagewebp',
					'\\imagecreatefromjpeg',
					'\\imagecreatefrompng',
					'\\imageistruecolor',
					'\\imagepalettetotruecolor',
					'\\imagealphablending',
					'\\imagesavealpha',
				);

				$this->can_generate_webp = true;
				foreach ( $functions as $function ) {
					if ( ! function_exists( $function ) ) {
						$this->can_generate_webp = false;

						break;
					}
				}
			}
			return $this->can_generate_webp;
		}

		/** Get transparency from file
		 *
		 * @param string $file_path to check.
		 * @param string $mime_type of image.
		 */
		public function get_image_file_transparency( $file_path, $mime_type ) {

			$is_transparent_image_file = null;
			if ( is_readable( $file_path ) && 'image/png' === $mime_type ) {

				try {
					$im_image = new \Imagick( $file_path );
					if ( $im_image instanceof \Imagick ) {
						$is_transparent_image_file = self::is_transparent( $im_image );
						$im_image->clear();
						$im_image->destroy();
						$im_image = null;
					}
				} catch ( Exception $e ) {
					Lib::error( 'Exception: ', $e->getMessage() );
				}
			}
			return $is_transparent_image_file;
		}

		/** Create webp clone.
		 *
		 * @param string $source_image to clone.
		 * @param string $mime_type of source image.
		 * @param bool   $transparency of image. Default null.
		 */
		public function webp_clone_image( $source_image, $mime_type = '', $transparency = null ) {

			if ( ! $this->can_generate_webp_clones() ) {
				return;
			}
			if ( ! is_readable( $source_image ) ) {
				return;
			}
			if ( ! is_string( $mime_type ) || ! trim( $mime_type ) ) {
				$mime_type = wp_check_filetype( $source_image );
				$mime_type = $mime_type['type'];
			}

			$webp_quality = 80;

			$gd_image = false;
			switch ( $mime_type ) {
				case 'image/jpeg':
					$webp_quality = $this->get_option( 'jpeg-compression-quality', 80 );
					$gd_image     = \imagecreatefromjpeg( $source_image );
					break;
				case 'image/png':
					if ( false === $transparency
					|| ( null === $transparency
						&& false === $this->get_image_file_transparency( $source_image, $mime_type ) ) ) {
						$gd_image = \imagecreatefrompng( $source_image );
						if ( $gd_image ) {
							imagealphablending( $gd_image, true );
							imagesavealpha( $gd_image, false );
							if ( ! imageistruecolor( $gd_image ) ) {
								imagepalettetotruecolor( $gd_image );
							}
						}
					}
					break;
			}

			$webp_file = self::get_webp_file_name( $source_image );
			if ( $gd_image ) {

				wp_mkdir_p( dirname( $webp_file ) );
				if ( \imagewebp( $gd_image, $webp_file, $webp_quality ) ) {
					if ( file_exists( $webp_file ) ) {

						if ( filesize( $webp_file ) % 2 === 1 ) {
							// phpcs:ignore
							file_put_contents( $webp_file, "\0", FILE_APPEND );
						}

						$stat  = stat( dirname( $webp_file ) );
						$perms = $stat['mode'] & 0000666;
						chmod( $webp_file, $perms );
					} else {
						Lib::debug( 'imagewebp: file not created' );
					}
				} else {
					Lib::debug( 'imagewebp: failed' );
					if ( file_exists( $webp_file ) ) {
						wp_delete_file( $webp_file );
					}
				}
				\imagedestroy( $gd_image );
			} else {

				if ( file_exists( $webp_file ) ) {
					wp_delete_file( $webp_file );
				}
			}
		}

		/** Resize image (maybe).
		 *
		 * @param string $source_file_path image file name to read.
		 * @param string $target_file_path image file name to write.
		 * @param int    $max_image_width to reduce target to.
		 *
		 * @return array|bool  geometry if image geometry changed, else false.
		 */
		private function check_resize_image_width( $source_file_path, $target_file_path, $max_image_width ) {

			$success = false;

			$resize_w = false;
			$target_w = false;

			$source_geometry = self::get_geometry( $source_file_path );

			if ( is_array( $source_geometry ) ) {

				if ( $max_image_width && $max_image_width < $source_geometry ['width'] ) {
					$resize_w = $max_image_width;
				}
			}

			if ( false !== $resize_w ) {

				$source_w = $source_geometry ['width'];
				$source_h = $source_geometry ['height'];

				if ( file_exists( $target_file_path ) ) {

					$target_geometry = self::get_geometry( $target_file_path );
					if ( is_array( $target_geometry ) ) {
						$target_w = $target_geometry ['width'];
					}
				}

				if ( $target_w !== $resize_w ) {

					try {

						$imagick = new \Imagick( $source_file_path );
						if ( $imagick instanceof \Imagick ) {

							$imagick->scaleImage( $resize_w, 0 );

							wp_mkdir_p( dirname( $target_file_path ) );
							if ( true === $imagick->writeImage( $target_file_path ) ) {
								$success = $imagick->getImageGeometry();
							}

							$imagick->clear();
							$imagick->destroy();
							$imagick = null;
						}
					} catch ( Exception $e ) {
						Lib::error( 'Exception: ', $e->getMessage() );
					}
				}
			}
			return $success;
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Static Helper functions

		/** Is transparent image?
		 *
		 * @param object $im_image to check for transparency.
		 */
		public static function is_transparent( $im_image ) {
			$is_transparent_image_obj = null;
			if ( $im_image instanceof \Imagick ) {
				try {
					$imgalpha = $im_image->getImageAlphaChannel();
					if ( $imgalpha ) {
						$img_mean = $im_image->getImageChannelMean( \Imagick::CHANNEL_ALPHA );
						if ( is_array( $img_mean ) ) {
							if ( ! in_array( $img_mean['mean'], array( 0.0, 1.0 ), true )
							&& ! in_array( $img_mean['standardDeviation'], array( 0.0, 1.0 ), true ) ) {
								$is_transparent_image_obj = true;
							} else {
								$is_transparent_image_obj = false;
							}
						}
					} else {
						$is_transparent_image_obj = false;
					}
				} catch ( Exception $e ) {
					Lib::error( 'Exception: ', $e->getMessage() );
				}
			}
			return $is_transparent_image_obj;
		}

		/** Check if metadata valid and contains required keys.
		 *
		 * @param array $metadata to check.
		 */
		private static function is_valid_metadata( $metadata ) {

			if ( ! is_array( $metadata ) ) {
				return false;
			}
			if ( ! array_key_exists( 'file', $metadata ) ) {
				return false;
			}
			if ( ! array_key_exists( 'width', $metadata ) ) {
				return false;
			}
			if ( ! array_key_exists( 'height', $metadata ) ) {
				return false;
			}
			return true;

		}

		/** Replace filename extension
		 *
		 * @param string $file_name_path to replace extension.
		 * @param string $extension to replace with.
		 */
		public static function replace_file_name_extension( $file_name_path, $extension ) {
			$pathinfo = pathinfo( $file_name_path );
			if ( array_key_exists( 'filename', $pathinfo ) ) {
				return ( array_key_exists( 'dirname', $pathinfo ) && '.' !== $pathinfo ['dirname'] ? trailingslashit( $pathinfo ['dirname'] ) : '' )
					. $pathinfo ['filename']
					. '.' . $extension;
			}
			return false;
		}

		/** Append filename extension
		 *
		 * @param string $file_name_path to append extension.
		 * @param string $extension to replace with.
		 */
		public static function append_file_name_extension( $file_name_path, $extension ) {
			$pathinfo = pathinfo( $file_name_path );
			if ( array_key_exists( 'basename', $pathinfo ) ) {
				return ( array_key_exists( 'dirname', $pathinfo ) && '.' !== $pathinfo ['dirname'] ? trailingslashit( $pathinfo ['dirname'] ) : '' )
					. $pathinfo ['basename']
					. '.' . $extension;
			}
			return false;
		}

		/** Get webp filename.
		 *
		 * @param string $source_file_name to convert to webp name.
		 */
		public static function get_webp_file_name( $source_file_name ) {
			return self::append_file_name_extension( $source_file_name, 'webp' );
		}

		/** Add suffix to filename
		 *
		 * @param string $file_name_path to receive suffix.
		 * @param string $suffix to append at the end of $file_name_path filename.
		 */
		public static function add_suffix_to_file_name( $file_name_path, $suffix ) {
			$pathinfo = pathinfo( $file_name_path );
			if ( array_key_exists( 'filename', $pathinfo ) ) {
				return ( array_key_exists( 'dirname', $pathinfo ) && '.' !== $pathinfo ['dirname'] ? trailingslashit( $pathinfo ['dirname'] ) : '' )
					. $pathinfo ['filename'] . $suffix
					. ( array_key_exists( 'extension', $pathinfo ) ? '.' . $pathinfo ['extension'] : '' );
			}
			return false;
		}

		/** Get file-image-geometry.
		 *
		 * @param string $image_file_name to get geometry from.
		 */
		public static function get_geometry( $image_file_name ) {

			$image_geometry = function_exists( '\\getimagesize' ) && file_exists( $image_file_name ) ? \getimagesize( $image_file_name ) : false;
			if ( is_array( $image_geometry )
			&& array_key_exists( 0, $image_geometry )
			&& array_key_exists( 1, $image_geometry ) ) {
				$image_w        = intval( $image_geometry [0] );
				$image_h        = intval( $image_geometry [1] );
				$image_geometry = array(
					'width'  => $image_w,
					'height' => $image_h,
				);
			} else {
				$image_geometry = false;
			}
			return $image_geometry;
		}

		/** Is file edited?
		 *
		 * Use file name to find out.
		 *
		 * @param string $image_file_path to check.
		 */
		public static function is_edited( $image_file_path ) {
			if ( preg_match( '/-e[0-9]{13}$/', pathinfo( $image_file_path, PATHINFO_FILENAME ) ) ) {
				return true;
			}
			return false;
		}

		/** Normalize file path/name.
		 *
		 * @param string $file_path_name - File path and name.
		 *
		 * @return string normalized path-name if file exists, else empty string.
		 */
		public static function normalize_path_name( $file_path_name ) {
			return realpath( wp_normalize_path( trim( '' . $file_path_name ) ) );
		}

		/** Get default jpeg quality. */
		public static function jpeg_quality_default() {
			return 50;
		}

		/** Get minimal jpeg quality. */
		public static function jpeg_quality_min_val() {
			return 30;
		}

		/** Get maximal jpeg quality. */
		public static function jpeg_quality_max_val() {
			return 85;
		}

		/** Get default max colors. */
		public static function png_max_colors_default() {
			return 1024;
		}

		/** Get minimal max colors. */
		public static function png_max_colors_min_val() {
			return 16;
		}

		/** Get maximal max colors. */
		public static function png_max_colors_max_val() {
			return 1024;
		}

		/** Get maximal max colors. */
		public static function png_max_colors_palette() {
			return 256;
		}

		/** Get default width limit. */
		public static function max_width_default() {
			return 2048;
		}

		/** Get minimal width limit. */
		public static function max_width_min_val() {
			return 768;
		}

		/** Get maximal width limit. */
		public static function max_width_max_val() {
			return 3000;
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Attachment Thumbnails Preview Template.

		/** Add Template Endpoint */
		public function add_template_endpoint() {
			\add_rewrite_endpoint( $this->get_slug(), EP_ALL );
			\add_action( 'wp', array( $this, 'handle_template_endpoint' ) );
		}

		/** Handle template endpoint. */
		public function handle_template_endpoint() {

			if ( $this->is_raw_image_template_request() ) {

				\remove_all_actions( 'template_redirect' );
				\add_action(
					'template_redirect', function() {
						\remove_all_filters( 'template_include' );
						\add_filter(
							'template_include', function( $template ) {

								$raw_image_template = $this->get_path() . '/templates/raw-image-template.php';
								if ( is_file( $raw_image_template ) ) {
									header( $this->get_slug() . ': template' );
									$template = $raw_image_template;
								} else {
									Lib::error( 'Template file not found: ' . $raw_image_template );
								}
								return $template;
							}
						);
						return false;
					}
				);
			}
		}

		/** Is Template Request */
		private function is_raw_image_template_request() {

			$my_wp_query = $GLOBALS['wp_the_query'];

			if ( ! isset( $my_wp_query->query_vars[ $this->get_slug() ] ) ) {
				return false;
			}

			if ( ! in_array( $my_wp_query->query_vars[ $this->get_slug() ], array( '', 'all', 'raw', 'full', 'webp' ), true ) ) {
				return false;
			}

			if ( ! isset( $my_wp_query->post->post_type ) || 'attachment' !== $my_wp_query->post->post_type ) {
				return false;
			}

			if ( ! isset( $my_wp_query->post->post_mime_type ) || ! Lib::starts_with( $my_wp_query->post->post_mime_type, 'image/' ) ) {
				return false;
			}

			return true;
		}

		// phpcs:ignore
	# endregion

		// phpcs:ignore
	# region Imagick Validation Arrays

		/** All Known & Defined Imagick Compression Types */
		public static function get_imagick_commpression_types() {

			$values = array();

			if ( defined( '\\Imagick::COMPRESSION_UNDEFINED' ) ) {
				$values [ \Imagick::COMPRESSION_UNDEFINED ] = __( 'UNDEFINED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_NO' ) ) {
				$values [ \Imagick::COMPRESSION_NO ] = __( 'DISABLED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_BZIP' ) ) {
				$values [ \Imagick::COMPRESSION_BZIP ] = __( 'BZIP', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_DXT1' ) ) {
				$values [ \Imagick::COMPRESSION_DXT1 ] = __( 'DXT1', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_DXT3' ) ) {
				$values [ \Imagick::COMPRESSION_DXT3 ] = __( 'DXT3', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_DXT5' ) ) {
				$values [ \Imagick::COMPRESSION_DXT5 ] = __( 'DXT5', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_FAX' ) ) {
				$values [ \Imagick::COMPRESSION_FAX ] = __( 'FAX', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_GROUP4' ) ) {
				$values [ \Imagick::COMPRESSION_GROUP4 ] = __( 'GROUP4', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_JPEG' ) ) {
				$values [ \Imagick::COMPRESSION_JPEG ] = __( 'JPEG', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_JPEG2000' ) ) {
				$values [ \Imagick::COMPRESSION_JPEG2000 ] = __( 'JPEG2000', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_LOSSLESSJPEG' ) ) {
				$values [ \Imagick::COMPRESSION_LOSSLESSJPEG ] = __( 'LOSSLESSJPEG', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_LZW' ) ) {
				$values [ \Imagick::COMPRESSION_LZW ] = __( 'LZW', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_RLE' ) ) {
				$values [ \Imagick::COMPRESSION_RLE ] = __( 'RLE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_ZIP' ) ) {
				$values [ \Imagick::COMPRESSION_ZIP ] = __( 'ZIP', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_ZIPS' ) ) {
				$values [ \Imagick::COMPRESSION_ZIPS ] = __( 'ZIPS', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_PIZ' ) ) {
				$values [ \Imagick::COMPRESSION_PIZ ] = __( 'PIZ', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_PXR24' ) ) {
				$values [ \Imagick::COMPRESSION_PXR24 ] = __( 'PXR24', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_B44' ) ) {
				$values [ \Imagick::COMPRESSION_B44 ] = __( 'B44', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_B44A' ) ) {
				$values [ \Imagick::COMPRESSION_B44A ] = __( 'B44A', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_LZMA' ) ) {
				$values [ \Imagick::COMPRESSION_LZMA ] = __( 'LZMA', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_JBIG1' ) ) {
				$values [ \Imagick::COMPRESSION_JBIG1 ] = __( 'JBIG1', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COMPRESSION_JBIG2' ) ) {
				$values [ \Imagick::COMPRESSION_JBIG2 ] = __( 'JBIG2', 'warp-imagick' );
			}

			return $values;
		}

		/** All Known & Defined Imagick Interlace Types */
		public static function get_imagick_interlace_types() {

			$values = array();

			if ( defined( '\\Imagick::INTERLACE_UNDEFINED' ) ) {
				$values [ \Imagick::INTERLACE_UNDEFINED ] = __( 'UNDEFINED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_NO' ) ) {
				$values [ \Imagick::INTERLACE_NO ] = __( 'DISABLED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_LINE' ) ) {
				$values [ \Imagick::INTERLACE_LINE ] = __( 'LINE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_PLANE' ) ) {
				$values [ \Imagick::INTERLACE_PLANE ] = __( 'PLANE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_PARTITION' ) ) {
				$values [ \Imagick::INTERLACE_PARTITION ] = __( 'PARTITION', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_GIF' ) ) {
				$values [ \Imagick::INTERLACE_GIF ] = __( 'GIF', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_JPEG' ) ) {
				$values [ \Imagick::INTERLACE_JPEG ] = __( 'JPEG', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::INTERLACE_PNG' ) ) {
				$values [ \Imagick::INTERLACE_PNG ] = __( 'PNG', 'warp-imagick' );
			}

			return $values;

		}

		/** All Known & Defined Imagick Colorspaces */
		public static function get_imagick_colorspaces() {

			$values = array();

			if ( defined( '\\Imagick::COLORSPACE_UNDEFINED' ) ) {
				$values [ \Imagick::COLORSPACE_UNDEFINED ] = __( 'UNDEFINED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_RGB' ) ) {
				$values [ \Imagick::COLORSPACE_RGB ] = __( 'RGB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_GRAY' ) ) {
				$values [ \Imagick::COLORSPACE_GRAY ] = __( 'GRAY', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_TRANSPARENT' ) ) {
				$values [ \Imagick::COLORSPACE_TRANSPARENT ] = __( 'TRANSPARENT', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_OHTA' ) ) {
				$values [ \Imagick::COLORSPACE_OHTA ] = __( 'OHTA', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LAB' ) ) {
				$values [ \Imagick::COLORSPACE_LAB ] = __( 'LAB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_XYZ' ) ) {
				$values [ \Imagick::COLORSPACE_XYZ ] = __( 'XYZ', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YCBCR' ) ) {
				$values [ \Imagick::COLORSPACE_YCBCR ] = __( 'YCBCR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YCC' ) ) {
				$values [ \Imagick::COLORSPACE_YCC ] = __( 'YCC', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YIQ' ) ) {
				$values [ \Imagick::COLORSPACE_YIQ ] = __( 'YIQ', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YPBPR' ) ) {
				$values [ \Imagick::COLORSPACE_YPBPR ] = __( 'YPBPR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YUV' ) ) {
				$values [ \Imagick::COLORSPACE_YUV ] = __( 'YUV', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_CMYK' ) ) {
				$values [ \Imagick::COLORSPACE_CMYK ] = __( 'CMYK', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_SRGB' ) ) {
				$values [ \Imagick::COLORSPACE_SRGB ] = __( 'SRGB - Default', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HSB' ) ) {
				$values [ \Imagick::COLORSPACE_HSB ] = __( 'HSB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HSL' ) ) {
				$values [ \Imagick::COLORSPACE_HSL ] = __( 'HSL', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HWB' ) ) {
				$values [ \Imagick::COLORSPACE_HWB ] = __( 'HWB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_REC601LUMA' ) ) {
				$values [ \Imagick::COLORSPACE_REC601LUMA ] = __( 'REC601LUMA', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_REC709LUMA' ) ) {
				$values [ \Imagick::COLORSPACE_REC709LUMA ] = __( 'REC709LUMA', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LOG' ) ) {
				$values [ \Imagick::COLORSPACE_LOG ] = __( 'LOG', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_CMY' ) ) {
				$values [ \Imagick::COLORSPACE_CMY ] = __( 'CMY', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LUV' ) ) {
				$values [ \Imagick::COLORSPACE_LUV ] = __( 'LUV', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HCL' ) ) {
				$values [ \Imagick::COLORSPACE_HCL ] = __( 'HCL', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LCH' ) ) {
				$values [ \Imagick::COLORSPACE_LCH ] = __( 'LCH', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LMS' ) ) {
				$values [ \Imagick::COLORSPACE_LMS ] = __( 'LMS', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LCHAB' ) ) {
				$values [ \Imagick::COLORSPACE_LCHAB ] = __( 'LCHAB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_LCHUV' ) ) {
				$values [ \Imagick::COLORSPACE_LCHUV ] = __( 'LCHUV', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_SCRGB' ) ) {
				$values [ \Imagick::COLORSPACE_SCRGB ] = __( 'SCRGB', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HSI' ) ) {
				$values [ \Imagick::COLORSPACE_HSI ] = __( 'HSI', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HSV' ) ) {
				$values [ \Imagick::COLORSPACE_HSV ] = __( 'HSV', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_HCLP' ) ) {
				$values [ \Imagick::COLORSPACE_HCLP ] = __( 'HCLP', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_YDBDR' ) ) {
				$values [ \Imagick::COLORSPACE_YDBDR ] = __( 'YDBDR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_REC601YCBCR' ) ) {
				$values [ \Imagick::COLORSPACE_REC601YCBCR ] = __( 'REC601YCBCR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_REC709YCBCR' ) ) {
				$values [ \Imagick::COLORSPACE_REC709YCBCR ] = __( 'REC709YCBCR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::COLORSPACE_XYY' ) ) {
				$values [ \Imagick::COLORSPACE_XYY ] = __( 'XYY', 'warp-imagick' );
			}

			return $values;

		}

		/** All Known & Defined Imagick Interlace Types */
		public static function get_imagick_imgtypes() {

			$values = array();

			if ( defined( '\\Imagick::IMGTYPE_UNDEFINED' ) ) {
				$values [ \Imagick::IMGTYPE_UNDEFINED ] = __( 'UNDEFINED', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_BILEVEL' ) ) {
				$values [ \Imagick::IMGTYPE_BILEVEL ] = __( 'BILEVEL', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_GRAYSCALE' ) ) {
				$values [ \Imagick::IMGTYPE_GRAYSCALE ] = __( 'GRAYSCALE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_GRAYSCALEMATTE' ) ) {
				$values [ \Imagick::IMGTYPE_GRAYSCALEMATTE ] = __( 'GRAYSCALEMATTE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_PALETTE' ) ) {
				$values [ \Imagick::IMGTYPE_PALETTE ] = __( 'PALETTE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_PALETTEMATTE' ) ) {
				$values [ \Imagick::IMGTYPE_PALETTEMATTE ] = __( 'PALETTEMATTE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_TRUECOLOR' ) ) {
				$values [ \Imagick::IMGTYPE_TRUECOLOR ] = __( 'TRUECOLOR', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_TRUECOLORMATTE' ) ) {
				$values [ \Imagick::IMGTYPE_TRUECOLORMATTE ] = __( 'TRUECOLORMATTE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_COLORSEPARATION' ) ) {
				$values [ \Imagick::IMGTYPE_COLORSEPARATION ] = __( 'COLORSEPARATION', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_COLORSEPARATIONMATTE' ) ) {
				$values [ \Imagick::IMGTYPE_COLORSEPARATIONMATTE ] = __( 'COLORSEPARATIONMATTE', 'warp-imagick' );
			}
			if ( defined( '\\Imagick::IMGTYPE_OPTIMIZE' ) ) {
				$values [ \Imagick::IMGTYPE_OPTIMIZE ] = __( 'OPTIMIZE', 'warp-imagick' );
			}

			return $values;

		}

		// phpcs:ignore
	# endregion

	}
}
