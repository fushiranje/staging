=== Warp iMagick - WordPress Image Compressor ===
Author: Dragan Đurić
Contributors: ddur
License: GPLv2
Requires PHP: 5.6
Tested up to: 5.5
Stable tag: 1.0.9
Requires at least: 4.7
Tags: optimize, pagespeed, performance, seo, webp
Donate link: https://www.etsy.com/shop/ZizouArT?ref=wordpress-warp-imagic-donate-yourself

Advanced image compression to thumbnails. Compress and convert to WebP clones. Disable 'big image size threshold', feature introduced in WP 5.3.

== Description ==

* **Optimize images, improve your Google Page Speed and SEO score with WebP images.**
* **Image optimizer that gives you full control over size and quality of images.**
* **Able to resize upload (original) images to maximum width (option).**
* **Regenerate thumbnails with 'wp media regenerate' command.**
* **Use only PHP software installed on your server.**
* **No executable binaries installed or required.**
* **No external optimization service required.**
* **No subscription email asked or required.**
* **No images sent to external server.**

= Features =

* **Easy to use: Install, activate, configure and done!**
After activation, only settings you may want to change is **JPEG Compression Quality**. For the rest of the settings, use defaults or feel free to experiment and have fun with. 😇

* **Automatic image optimization of PNG & JPEG thumbnails and convert to WebP clones:**
Images will be automatically compressed on upload or on "regenerate thumbnails". Original image is preserved. Compression will always start from original image quality, never "overoptimize". Reoptimize existing images with WP CLI or ["Regenerate Thumbnails"](https://wordpress.org/plugins/regenerate-thumbnails/) plugin batch process.

* **Compatible with ["WP CLI media regenerate" command](https://developer.wordpress.org/cli/commands/media/regenerate/) and with ["Regenerate Thumbnails" plugin](https://wordpress.org/plugins/regenerate-thumbnails/).**
**Important Note:** Since WordPress 5.3, BIG JPEG images reduced to 2560x2560 (by "Big Image Size Threshold" feature) and then manually edited after upload, will be regenerated back to upload version. See [GitHub issue](https://github.com/Automattic/regenerate-thumbnails/issues/102).

* **Compatible up to PHP 7 and tested against coding standards.**
Tested with [PHP_CodeSniffer (phpcs)](https://github.com/squizlabs/PHP_CodeSniffer) using [WordPress Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/php/) [GitHub](https://github.com/WordPress/WordPress-Coding-Standards) rules and [PHPCompatibility](https://github.com/PHPCompatibility/PHPCompatibility) rules, on PHP 7.4.9 .

* **New in 1.0.8: Option to disable "Big Image Size Threshold" feature/filter**
Prevent WordPress from downsizing of BIG JPEG images (larger than 2560x2560 pixels) and compress them to "thumbnail quality".

* **JPEG Compression Quality:**
Select [JPEG compression quality](https://developers.google.com/speed/docs/insights/OptimizeImages#optimizations-for-gif,-png,-and-jpeg-images) from 30% to 85%. Current WordPress default is 82%. [Lossy Compression].

* **JPEG Compression Type:**
WordPress Default or Imagick Default. [Lossy Compression].

* **JPEG Colorspace Transform:**
WordPress Default, RGB, [sRGB*](https://developers.google.com/speed/docs/insights/OptimizeImages#optimizations-for-gif,-png,-and-jpeg-images), scRGB, LOG or GRAY. Default sRGB colorspace is a [**World Wide Web Standard**](https://www.w3.org/Graphics/Color/sRGB.html).

* **JPEG Color Sampling Factors:**
WordPress Default, 4:1:0, 4:1:1, [4:2:0*](https://developers.google.com/speed/docs/insights/OptimizeImages#optimizations-for-gif,-png,-and-jpeg-images), 4:2:1, 4:2:2, 4:4:0, 4:4:1, 4:4:4. [Lossy Color Compression].

* **JPEG Interlace Scheme:**
WordPress Default, No Interlace, Imagick Default, Progressive, Auto Select* - Compare "No Interlace" with "Progressive" file size and save smaller file to the disk. [Loosless Compression].

* **PNG Color Reduction:**
Quantize PNG Colors to range between 16 and 1024 colors. [Lossy Color Compression].

* **PNG Color Dithering:**
Enable Color Dithering to improve color transition quality (except transparent & less-than-257-colors). [Lossy Compression].

* **PNG Color Palette:**
Images reduced to less than 257 colors are automatically converted to PNG Color Palette. [Lossy Color Compression].

* **PNG Compression:**
WordPress Default and Imagick Default compression strategies are compared and smallest file size is written to disk. [Loosless Compression].

* **WebP Conversion:**
Enable to automatically generate optimized WebP versions of JPEG and PNG (except transparent) images & thumbnails. See Settings page "Help" how about to configure server.

* **Strip Metadata:**
WordPress Default settings, force WP Default Off, force WP Default On, Strip All Metadata* (on JPEG: only if colorspace is sRGB, else uses WP strip metadata). [Loosless Compression].

* **Maximum Upload Width**
Resize Large Upload/Original Images to maximum width.

* **Clean uninstall:**
By default, nothing is left in your database after uninstall. You may choose not to remove plugin settings on uninstall. Feel free to install and activate to make a trial of this plugin functionality. However, you can choose to preserve plugin options after uninstall.

* **Privacy**
This plugin does not collect nor send any personally identifiable data. WordPress builtin cookies are used to store admin-settings page-state.

* **Multisite support**
Designed to work on WP multisite. No known reason to fail on WP multisite, but not extensively tested yet!

== Installation ==

= Using The WordPress Plugin Repository =
1. Navigate to the 'Plugins' -> 'Add New' .
2. Search for 'Warp iMagick'.
3. Select and click 'Install Now'.
4. Activate the plugin.

== Upgrade Notice ==
None.

== Screenshots ==
1. **JPEG Settings**
2. **PNG Settings**
3. **WebP Settings**
4. **Other Settings**
5. **Regenerate Thumbnails**
6. **WebP Mobile Page Score**

== Frequently Asked Questions ==

= Which PHP extensions are required by this plugin? =
1. PHP-Imagick to compress JPEG/PNG files (required).
2. PHP-GD for WebP files (optional, but usually installed).

In order to modify/resize/crop photos or images in Wordpress, at least PHP-GD to extension is required. Wordpress supports image editing and resizing only with two above listed extensions. When both extensions are installed, WordPress prefers PHP-Imagick over PHP-GD.

= Do I have both required PHP extensions installed? =
1. WordPress 5.2 and above: Administrator: Menu -> Tools -> Site Health -> Info -> Expand "Media Handling" and check if "ImageMagick version string" and "GD version" have values.
2. WordPress 5.1 and below: Install [Health Check & Troubleshooting](https://wordpress.org/plugins/health-check/) plugin. Open "Health Check" plugin page and click on "PHP Information" tab. You will find there all PHP extensions installed and enabled. Search (Ctrl-F) on page for "Imagick" and "GD".
3. WordPress Editor class must be WP_Image_Editor_Imagick (or Warp_Image_Editor_Imagick) but **NOT** WP_Image_Editor_GD.
4. PHP-Imagick extension must be linked with ImageMagick library version **6.3.2** or newer.
5. PHP-GD extension version must be at least 2.0.0 to be accepted by WordPress Image Editor.

= Does my web hosting service provide PHP Imagick and GD extensions? =
1. [WPEngine](https://wpengine.com/support/platform-settings/): Both by default.
2. [EasyWP](https://www.namecheap.com/support/knowledgebase/article.aspx/9697/2219/php-modules-and-extensions-on-shared-hosting-servers): Both by default.
3. [DreamHost](https://help.dreamhost.com/hc/en-us/articles/214893957): By configuration.
4. [SiteGround](https://www.siteground.com/kb/enable-imagick-imagemagick/): Must enable.
5. Ask your host-service provider.

= How to install missing PHP-Imagick extension? =
1. [PHP-Imagick setup](https://www.php.net/manual/en/imagick.setup.php)
2. [CPanel based host](https://documentation.cpanel.net/display/68Docs/PHP+Extensions+and+Applications+Package#PHPExtensionsandApplicationsPackage-PHPExtensionsandApplicationsPackageInstaller)
3. Debian/Ubuntu: "apt-get install php-imagick".
4. Fedora/CentOs: "yum install php-imagick".
5. Ask your host-service provider.

= How to serve WebP images? =
See plugin **HELP** for instructions how to configure server to redirect .jpg/.png to .jpg.webp/.png.webp, if such file exists and browser suports WebP image format.

= Why WebP files have two extensions? =
To prevent overwriting duplicate "WebP" files. With single extension, when you upload "image.png" and "image.jpg", second "image.webp" would overwrite previous one.

= Why is WebP (checkbox) disabled? =
Because your server has no PHP-GD graphic editing extension or PHP-GD extension has no WebP support.

= What happens with images when plugin is disabled or deleted? =
1. If WebP was never enabled, existing images remain optimized. New media thumbnails won't be optimized. If you run ["Regenerate Thumbnails"](https://wordpress.org/plugins/regenerate-thumbnails/) batch process, it will restore original file-size and quality of WordPress thumbnails.
2. If you have WebP images, they won't be deleted. You should delete all WebP images before deactivate/delete this plugin. To delete WebP images, first disable WebP option and then batch-run ["Regenerate Thumbnails"](https://wordpress.org/plugins/regenerate-thumbnails/) for all media images.

= Why plugin fails to activate on my server? =
Because your server has not PHP-Imagick installed or has too old version of PHP-Imagick. Maybe some other plugin creates conflict.

== Changelog ==

= 1.0.9 =
* If plugin activation prerequisite is missing, disable plugin and show error message.

= 1.0.8.1 =
* Extend PHP max_execution_time if available. Last minute fix.

= 1.0.8 =
* Option to disable "big_image_size_threshold" filter. Default is "on".
* Execute "Strip All" metadata on JPEG, only when colorspace is sRGB.
* "Site info" tab, showing registered image sizes.
* Clone [original_image] to WebP, when exists.
* Extend PHP max_execution_time if set.

= 1.0.7 =
* Improved resilience. Second argument in "intermediate_image_sizes_advanced" filter is now optional.

= 1.0.6 =
* Regenerate is no more detectable since WP 5.3+. Process for Upload and Regenerate is now same. Obsolete code removed. Compatibility improved.

= 1.0.5 =
* Fix transparency-check after edit/restore.
* Cover transparency-check exception.
* Hooks refactored.

= 1.0.4 =
* Better transparency detection

= 1.0.3 =
* Do not generate WebP for transparent PNG images

= 1.0.2 =
* Do not dither transparent PNG images

= 1.0.1 =
* Added PNG Reduction & Generate WebP Images

= 1.0.0 =
* Initial WordPress.org Release

