<?php
/**
 * Copyright © 2017-2020 Dragan Đurić. All rights reserved.
 *
 * @package warp-imagick
 * @license GNU General Public License Version 2.
 * @copyright © 2017-2020 Dragan Đurić. All rights reserved.
 * @author Dragan Đurić <dragan dot djuritj at gmail dot com>
 * @link https://wordpress.org/plugins/warp-imagick/
 *
 * This copyright notice, source files, licenses and other included
 * materials are protected by U.S. and international copyright laws.
 * You are not allowed to remove or modify this or any other
 * copyright notice contained within this software package.
 */

/**
 * Custom Template for Media Attachment Images
 *
 * @package Warp iMagick
 * @version 1.0
 */
namespace ddur\Warp_iMagick;

use \ddur\Warp_iMagick\Base\Plugin\v1\Lib;

/** Get array of media attachment images
 *
 * @param int $id of attachment.
 * @return array|false [$size-name => $file-path]
 */
function get_attachment_image_files( $id ) {

	$img_meta = \wp_get_attachment_metadata( $id );
	if ( ! is_array( $img_meta ) ) {
		return false;
	}
	if ( ! isset( $img_meta ['file'] ) ) {
		return false;
	}
	if ( ! is_string( $img_meta ['file'] ) ) {
		return false;
	}
	if ( ! isset( $img_meta ['sizes'] ) ) {
		return false;
	}
	if ( ! is_array( $img_meta ['sizes'] ) ) {
		return false;
	}

	$my_wp_query = $GLOBALS['wp_the_query'];
	$query_value = strtolower( $my_wp_query->query_vars[ Plugin::slug() ] );
	$query_value = '' !== trim( $query_value ) ? $query_value : 'all';

	$main_img_path = \trailingslashit( \wp_upload_dir() ['basedir'] ) . $img_meta ['file'];
	$base_dir_path = \trailingslashit( dirname( $main_img_path ) );

	$distinct_files = array();

	if ( in_array( $query_value, array( 'raw', 'all', 'webp' ), true ) ) {
		foreach ( $img_meta ['sizes'] as $img_meta_size => $img_meta_data ) {
			$img_meta_size = str_replace( array( '_', '-' ), ' ', $img_meta_size );
			if ( in_array( $query_value, array( 'raw', 'all' ), true ) ) {
				$distinct_files [ $base_dir_path . $img_meta_data ['file'] ] = array( $img_meta_size );
			}
			if ( in_array( $query_value, array( 'all', 'webp' ), true ) ) {
				$distinct_files [ $base_dir_path . Plugin::get_webp_file_name( $img_meta_data ['file'] ) ] = array( $img_meta_size . ' (webp)' );
			}
		}
	}
	if ( in_array( $query_value, array( 'full', 'all' ), true ) ) {
		$distinct_files[ $main_img_path ] = array( 'full size' );
	}
	if ( in_array( $query_value, array( 'full', 'all', 'webp' ), true ) ) {
		$distinct_files [ Plugin::get_webp_file_name( $main_img_path ) ] = array( 'full size (webp)' );
	}

	if ( \function_exists( '\\wp_get_original_image_path' ) && ! empty( $img_meta ['original_image'] ) ) {
		$orig_img_path = \wp_get_original_image_path( $id );

		if ( in_array( $query_value, array( 'full', 'all' ), true ) ) {
			$distinct_files[ $orig_img_path ] = array( 'original upload' );
		}

		if ( in_array( $query_value, array( 'full', 'all', 'webp' ), true ) ) {
			$distinct_files [ Plugin::get_webp_file_name( $orig_img_path ) ] = array( 'original upload (webp)' );
		}
	}

	$existing_files = array();

	foreach ( $distinct_files as $file_path => $size_data ) {
		if ( file_exists( $file_path ) && Lib::starts_with( mime_content_type( $file_path ), 'image/' ) ) {

			$image_file_size = intval( \filesize( $file_path ) );
			$image_geometry  = \getimagesize( $file_path );
			$size_name_width = 0;
			if ( is_array( $image_geometry )
			&& array_key_exists( 0, $image_geometry ) ) {
				$size_name_width = intval( $image_geometry [0] );
			}
			$size_data[1] = $size_name_width;
			$size_data[2] = $image_file_size;

			$existing_files [ $file_path ] = $size_data;
		}
	}

	$distinct_files = $existing_files;

	uksort(
		$distinct_files, function( $a, $b ) use ( $distinct_files ) {

			$width_a = intval( $distinct_files [ $a ] [1] );
			$width_b = intval( $distinct_files [ $b ] [1] );
			if ( $width_a === $width_b ) {
				return 0;
			} elseif ( $width_a > $width_b ) {
				return 1;
			} else {
				return -1;
			}
		}
	);

	$files = array();
	foreach ( $distinct_files as $file_path => $size_data ) {
		if ( file_exists( $file_path ) && Lib::starts_with( mime_content_type( $file_path ), 'image/' ) ) {
			$files [ $size_data[0] ] = $file_path;
		}
	}
	return $files;
}

/** Return <img> elements */
function get_img_html_elements() {

	if ( ! is_callable( '\\getimagesize' ) ) {
		Lib::error( 'Function is not callable: \getimagesize' );
		return '';
	}

	$my_wp_query = $GLOBALS['wp_the_query'];
	$img_files   = get_attachment_image_files( $my_wp_query->post->ID );

	if ( ! is_array( $img_files ) || count( $img_files ) === 0 ) {
		return '';
	}

	$html      = '';
	$break     = 0;
	$root_path = wp_normalize_path( untrailingslashit( ABSPATH ) );
	foreach ( $img_files as $size_name => $file_path ) {
		$file_path = \wp_normalize_path( $file_path );
		$file_size = \size_format( \filesize( $file_path ) );
		$size_data = \getimagesize( $file_path );
		if ( is_array( $size_data ) && count( $size_data ) > 1 ) {

			if ( Lib::starts_with( $file_path, $root_path ) ) {
				if ( $break ) {
					if ( $break !== $size_data [0] ) {
						$break = $size_data [0];
						$html .= '<br>' . PHP_EOL;
					}
				} else {
					$break = $size_data [0];
				}
				$file_size = esc_attr( $file_size );
				$size_name = esc_attr( $size_name );
				$src       = esc_url_raw( substr( $file_path, strlen( $root_path ) ) );
				$width     = esc_attr( $size_data [0] );
				$height    = esc_attr( $size_data [1] );
				$basename  = basename( $file_path );
				$title     = esc_attr( "Size name: $size_name\nFile name: $basename\nFile size: $file_size" );
				$html     .= "<img data-file-size='$file_size' data-size-name='$size_name' src='$src' width='$width' height='$height' title='$title'>" . PHP_EOL;
			}
		}
	}
	return $html;
}
?><!doctype html><html><body>
<?php Lib::echo_html( get_img_html_elements() ); ?></body></html>
