<?php

function smartads_register_post_type() {

$labels = array(
    'name' => __( 'Smart Ads' , 'smartads' ),
    'singular_name' => __( 'Smart Ad' , 'smartads' ),
    'add_new' => __( 'New Smart Ad' , 'smartads' ),
    'add_new_item' => __( 'Add New ad' , 'smartads' ),
    'edit_item' => __( 'Edit Ad' , 'smartads' ),
    'new_item' => __( 'New Ad' , 'smartads' ),
    'view_item' => __( 'View Ad' , 'smartads' ),
    'search_items' => __( 'Search Ads' , 'smartads' ),
    'not_found' =>  __( 'No Ads Found' , 'smartads' ),
    'not_found_in_trash' => __( 'No Ads found in Trash' , 'smartads' ),
);

$args = [
    'labels' => $labels,
    'has_archive' => true,
    'public' => true,
    'hierarchical' => false,
    'supports' => [
    'title',
    "url",
    //			'editor',
    //			'excerpt',
    //			'custom-fields',
    'gallery',
    'gallery_mobile',
    'revisions',
    'page-attributes'
    ],
    'rewrite'   => ['slug' => 'smartads'],
    'show_in_rest' => true
];

register_post_type( 'smartads', $args);
}

add_action( 'init', 'smartads_register_post_type' );

