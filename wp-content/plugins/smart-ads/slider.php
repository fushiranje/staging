<?php

	

	$_SESSION["excludeAds"] = array();
	function slider_styles_admin()
	{
		wp_register_style(
			'awp-block-slider-style',
			plugins_url('assets/css/editor_block_slider.css', __FILE__),
			['wp-edit-blocks']
		);

		wp_register_script(
			'awp-block-slider-js',
			plugins_url('assets/js/block_slider.js', __FILE__),
			['wp-blocks', 'wp-element', 'wp-editor', 'wp-components', 'wp-data']
		);
		wp_enqueue_script('awp-block-slider-js');

	}
	add_action('enqueue_block_editor_assets', 'slider_styles_admin');


	function register_my_block_1($iteration)
	{
		register_block_type('awp/slider', [
			'editor_script' => 'awp-block-slider-js',
			'editor_style' => 'awp-block-slider-style',
			'render_callback' => 'awp_gutenberg_slider_render',
		]);
	}
	add_action( 'init', 'register_my_block_1' );

	function awp_gutenberg_slider_render() {
        // echo "aaaaaaaaaaaaaaaaaaaaaaaaa";
		$args = array(
			'post_type' => 'smartads',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		);

		$postQuery = new WP_Query($args);
		$posts = $postQuery->get_posts();
		$postsCount = !empty($posts)?count($posts)-1:0;


		$_SESSION["excludeAds"] = !empty($_SESSION["excludeAds"])?$_SESSION["excludeAds"]:array();

		$random_number = randWithout(0,$postsCount, $_SESSION["excludeAds"]);
        if (count($_SESSION["excludeAds"]) >= $postsCount-1) {
            $_SESSION["excludeAds"] = array();
        }

		$_SESSION["excludeAds"][$random_number] = $random_number;

		$ad = !empty($posts[$random_number])?$posts[$random_number]:array();
        // var_dump($ad);
        // die();
		if (!empty($ad))
		{
            $options = get_option( 'dbi_smart_ads_options' );
			
            $cycleTimeout = !empty($options["slider_seconds"])?intval($options["slider_seconds"]) * 1000:4000;

            $ad_url = get_post_meta( $ad->ID, 'ad_url', true );
			$ad_meta = get_post_meta( $ad->ID, 'gallery_data', true );

			$output = "";
			if (!empty($ad_meta["image_url"]))
			{
                $output .= '<a href="'.$ad_url.'" target="_blank">';
                    $output .= '<div class="wp-block-awp-slider aligncenter" style="width:100%; height=300px;">';
                        $output .= '<div class="cycle-slideshow" data-cycle-timeout='.$cycleTimeout.'>';
                        foreach($ad_meta["image_url"] as $url)
                        {
                            $output .= '<img src="' . $url . '" style="width:100%; height=300px;"/>';
                        }
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '<a/>';
			}
           
			return $output;

		} else {
			return '';
		}
	}

	function slider_styles_frontend()
	{
		wp_enqueue_script(
			'cycle2-slider-js',
			plugins_url('assets/js/jquery.cycle2.min.js', __FILE__),
			['jquery'],
			'',
			true
		);
	}
	add_action('wp_enqueue_scripts', 'slider_styles_frontend');
