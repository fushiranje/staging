<?php

function dbi_add_settings_page() {
    add_options_page( 'Smart Ad\'s Settings', 'Smart Ad\'s Settings', 'manage_options', 'dbi-smart-ads', 'dbi_render_plugin_settings_page' );
}
add_action( 'admin_menu', 'dbi_add_settings_page' );

function dbi_render_plugin_settings_page() {
    ?>
    <h2>Smart Ad's Settings</h2>
    <form action="options.php" method="post">
        <?php 
        settings_fields( 'dbi_smart_ads_options' );
        do_settings_sections( 'dbi_smart_ads' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
    <?php
}

function dbi_register_settings() {
    register_setting( 'dbi_smart_ads_options', 'dbi_smart_ads_options', 'dbi_smart_ads_options_validate' );
    add_settings_section( 'api_settings', 'Smart ad\'s Settings', 'dbi_plugin_section_text', 'dbi_smart_ads' );

    add_settings_field( 'dbi_plugin_setting_slider_seconds', 'Seconds per slide', 'dbi_plugin_setting_slider_seconds', 'dbi_smart_ads', 'api_settings' );
    // add_settings_field( 'dbi_plugin_setting_results_limit', 'Results Limit', 'dbi_plugin_setting_results_limit', 'dbi_smart_ads', 'api_settings' );
    // add_settings_field( 'dbi_plugin_setting_start_date', 'Start Date', 'dbi_plugin_setting_start_date', 'dbi_smart_ads', 'api_settings' );
}
add_action( 'admin_init', 'dbi_register_settings' );

function dbi_smart_ads_options_validate( $input ) {
    $newinput['slider_seconds'] = trim( $input['slider_seconds'] );

    return $newinput;
}


function dbi_plugin_section_text() {
    echo '<p>Here you can set all the options for Smart Ad\'s</p>';
}

function dbi_plugin_setting_slider_seconds() {
    $options = get_option( 'dbi_smart_ads_options' );
    // print_r($options);
    // die();
    echo "<input id='dbi_plugin_setting_slider_seconds' name='dbi_smart_ads_options[slider_seconds]' type='number' min=\"1\" max=\"120\" value='" . esc_attr( $options['slider_seconds'] ) . "' />";
}

// function dbi_plugin_setting_results_limit() {
//     $options = get_option( 'dbi_smart_ads_options' );
//     echo "<input id='dbi_plugin_setting_results_limit' name='dbi_smart_ads_options[results_limit]' type='text' value='" . esc_attr( $options['results_limit'] ) . "' />";
// }

// function dbi_plugin_setting_start_date() {
//     $options = get_option( 'dbi_smart_ads_options' );
//     echo "<input id='dbi_plugin_setting_start_date' name='dbi_smart_ads_options[start_date]' type='text' value='" . esc_attr( $options['start_date'] ) . "' />";
// }