/* block.js */
var el = wp.element.createElement;

wp.blocks.registerBlockType('smart-ad/placeholder-block', {

	title: 'Smart Ad', // Block name visible to user

	icon: 'info-outline', // Toolbar icon can be either using WP Dashicons or custom SVG

	category: 'embed', // Under which category the block would appear

	attributes: { // The data this block will be storing

		// type: { type: 'string', default: 'default' }, // Notice box type for loading the appropriate CSS class. Default class is 'default'.

		// title: { type: 'string' }, // Notice box title in h4 tag

		// content: { type: 'array', source: 'children', selector: 'p' } /// Notice box content in p tag

	},

	edit: function(props) {
		// How our block renders in the editor in edit mode

		// function updateTitle( event ) {
		// 	props.setAttributes( { title: event.target.value } );
		// }
		//
		// function updateContent( newdata ) {
		// 	props.setAttributes(  );
		// }
		//
		// function updateType( event ) {
		// 	props.setAttributes( { type: event.target.value } );
		// }

		return el( 'div',
			{
				className: 'notice-box'
			},
			el('figure',
				{className: 'wp-block-image'},
				el('img', {src: "https://fakeimg.pl/500x200/?text=SmartAd"})
			)
			// el( https://via.placeholder.com/350x150
			// 	'input',
			// 	{
			// 		type: 'text',
			// 		placeholder: 'Enter title here...',
			// 		value: props.attributes.title,
			// 		onChange: updateTitle,
			// 		style: { width: '100%' }
			// 	}
			// ),
			// el(
			// 	wp.editor.RichText,
			// 	{
			// 		tagName: 'p',
			// 		onChange: updateContent,
			// 		// value: props.attributes.content,
			// 		placeholder: 'Enter description here...'
			// 	}
			// )
		); // End return
	},

	save: function(props) {
		// How our block renders on the frontend
        return null; // Nothing to save here..
	}
});