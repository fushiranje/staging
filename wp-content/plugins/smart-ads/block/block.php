<?php

function smart_ad_admin()
{
	$allAds = getAllAds();
//	print_r($allAds);

	wp_register_script(
		'smart-ad-editor',
		plugins_url('block.js', __FILE__),
		array('wp-blocks', 'wp-element')
	);

	wp_localize_script(
		'smart-ad-editor',
		"testObject",
		$allAds
	);

	wp_enqueue_script('smart-ad-editor');

	wp_enqueue_style(
		'smart-ad-editor',
		plugins_url('block.css', __FILE__),
		array()
	);
}

add_action('enqueue_block_editor_assets', 'smart_ad_admin');

// Load assets for frontend
function smart_ad_frontend()
{
	wp_enqueue_style(
		'smart-ad-editor',
		plugins_url('block.css', __FILE__),
		array()
	);
}

add_action('wp_enqueue_scripts', 'smart_ad_frontend');