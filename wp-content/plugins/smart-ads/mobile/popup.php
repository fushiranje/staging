<?php


	function modal_styles_frontend()
	{
		wp_enqueue_script(
			'popup',
			plugins_url('../assets/js/popup.js', __FILE__),
			['jquery'],
			'',
			true
		);
        $output = boeckske_dialog_html();
        $adData = json_encode($output);
        // echo "aaaaaaaaaaaa";
        // print_r($adData);
        wp_localize_script( 'popup', 'adData', $adData );
	}


	add_action('wp_enqueue_scripts', 'modal_styles_frontend');


    function boeckske_dialog_html() {

        $response = array(
            "html" => "",
        );


        $args = array(
			'post_type' => 'smartads',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		);

		$postQuery = new WP_Query($args);
		$posts = $postQuery->get_posts();
		$postsCount = !empty($posts)?count($posts)-1:0;
        // $exclude = array();

        // $random_number = randWithout(0,$postsCount, $exclude);
        // if (count($exclude) >= $postsCount-1)
        // {
        //     $exclude = array();
        // }
        // $exclude[$random_number] = $random_number;


		// $postsCount = !empty($posts)?count($posts)-1:0;
		
		// $ad = !empty($posts[$random_number])?$posts[$random_number]:array();

		if (!empty($posts))
		{
            
            foreach ($posts as $key => $ad) 
            {

                $options = get_option( 'dbi_smart_ads_options' );
                
                $cycleTimeout = !empty($options["slider_seconds"])?intval($options["slider_seconds"]) * 1000:4000;

                $ad_url = get_post_meta( $ad->ID, 'ad_url', true );
                $ad_meta = get_post_meta( $ad->ID, 'gallery_data_mobile', true );
                $metaKey = "image_url_mobile";
                // echo "aaaaaaaaaaaaa";
                // var_dump($ad_meta);
                if (empty($ad_meta))
                {
                    $ad_meta = get_post_meta( $ad->ID, 'gallery_data', true );
                    $metaKey = "image_url";
                }
                $output = "";
                if (!empty($ad_meta[$metaKey]))
                {
                    $output .= '<div class="modal-dialog modal-lg">';
                        $output .= '<div class="modal-content">';
                            
                            $output .= '<div class="modal-header">';
                                $output .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">';
                                    $output .= '<i class="material-icons">X</i>';
                                $output .= '</button>';
                            //    $output .= ' <h4 class="modal-title">Modal title</h4>';
                            $output .= '</div>';
                          
                            $output .= '<a href="'.$ad_url.'" target="_blank">';
                                $output .= '<div id="home_slider" class="wp-block-awp-slider aligncenter" style="width:100%; height=300px;">';
                                    $output .= '<div class="cycle-slideshow" data-cycle-timeout='.$cycleTimeout.'>';
                                    foreach($ad_meta[$metaKey] as $url)
                                    {
                                        $output .= '<img src="' . $url . '" style="width:100%; height=300px;"/>';
                                    }
                                    $output .= '</div>';
                                $output .= '</div>';
                            $output .= '<a/>';

                        $output .= '</div>';
                    $output .= '</div>';
                        
                }

                $response[$key]["html"] = $output;
                $response[$key]["adMeta"] = $output;
                // echo $output;
            }
            
        }
        $response["postCount"] = $postsCount-1;
        
        return $response;
	}


    function injectInitialInFooter()
    {
        $output = '<div class="modal" id="myModal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">';
        $output .= '</div>';
        echo $output;
    }

    add_action( 'wp_footer', 'injectInitialInFooter' );