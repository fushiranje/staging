<?php
add_action('admin_init', 'my_theme_on_admin_init');

function my_theme_on_admin_init() {
	add_meta_box('ad_url',
		__('Ad url', 'textdomain'),
		'ad_render',
		'smartads', 'normal', 'high'
	);
}

function ad_render($post) {
	$data = get_post_meta($post->ID, 'ad_url', true);
	// Use nonce for verification
	wp_nonce_field('add_smart_ad', 'smart_ad_nonce');
	?>
	<div class="inside">
		<table class="form-table">
			<tr valign="top">
				<th scope="row">
					<label for="ad_url"><?php _e('Ad Url', 'textdomain'); ?></label>
				</th>
				<td>
					<input id="ad_url" name="ad_url" value="<?php echo (isset($data)) ? $data : ''; ?>">
				</td>
			</tr>
		</table>
	</div>
	<?php
}

add_action('wp_insert_post', 'save_my_meta', 10, 2);

function save_my_meta($id) {
//	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//		return $id;

	$is_autosave = wp_is_post_autosave( $id );
	$is_revision = wp_is_post_revision( $id );
	$is_valid_nonce = ( isset( $_POST[ 'sample_nonce' ] ) && wp_verify_nonce( $_POST[ 'sample_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	if (!current_user_can('edit_posts'))
		return;

	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}
	if ( ! current_user_can( 'edit_post', $id ) ) {
		return;
	}

	if (!isset($id))
		$id = (int) $_REQUEST['post_ID'];

	if (isset($_POST['ad_url']))
	{
		$data = $_POST['ad_url'];

		if (isset($data)) {
			update_post_meta($id, 'ad_url', $data);
		}
		else {
			delete_post_meta($id, 'ad_url');
		}
	}
}