<?php
/**
 * @package SmartAds
 */

/*

Plugin Name: Smart Ads

Description: Smarter way to advertise

Version: 0.0.2

Author: Josip H.

License: GPLv2 or later

Text Domain: smartads

*/

    // if ( ! defined( 'WPINC' ) ) {
    // 	die;
    // }

    /** 
     * include main script
     */
    $options = get_option( 'dbi_smart_ads_options' );
    include(__DIR__."/main.php");

    include(__DIR__."/field/url.php");
    include(__DIR__."/gallery/gallery.php");
    include(__DIR__."/gallery/gallery_mobile/gallery.php");

    /** 
     * Slider include
     */

    include(__DIR__."/slider.php");
    
    // /** 
    //  * include placeholder block for plugin
    //  */
    // include(__DIR__."/block/block.php");
    
    /**
     * Include settings for the plugin
     */
    include(__DIR__."/settings/settings.php");
    
    include(__DIR__."/post-ads/post_ads.php");
    
    include(__DIR__."/mobile/popup.php");

    function getAllAds()
    {
        $args = array(
            'post_type' => 'smartads',
            'posts_per_page' => -1,
            'post_status' => 'publish',
        );

        $query = new WP_Query;
        return $query->query( $args );
    }

    function randWithout($from, $to, array $exceptions) {
        sort($exceptions); // lets us use break; in the foreach 
        // $exceptions[] = 0;
        // print_r($exceptions);
        $i = 0;
        while(true)
        {
            $number = rand($from, $to); // or mt_rand()
            if (in_array($number, $exceptions)) {
                $number = rand($from, $to);
                $i++;
                if ($i > 20)
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
      
        return $number;
    }

