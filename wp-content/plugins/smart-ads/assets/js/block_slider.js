/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!***************************************!*\
  !*** ./gutenberg/src/block_slider.js ***!
  \***************************************/
var __ = wp.i18n.__;
var registerBlockType = wp.blocks.registerBlockType;
var Placeholder = wp.components.Placeholder;

var BlockEdit = function BlockEdit(props) {
  return /*#__PURE__*/React.createElement("div", {
    className: props.className
  }, /*#__PURE__*/React.createElement("img", {
    src: "https://fakeimg.pl/1280x350"
  }));
};

registerBlockType('awp/slider', {
  title: __('Smart Ad', 'awp'),
  icon: 'slides',
  category: 'embed',
  supports: {
    align: ['center', 'wide', 'full']
  },
  attributes: {
    adBlockId: {
      type: 'number',
      "default": 0
    }
  },
  edit: BlockEdit,
  save: function save() {
    return null;
  }
});
/******/ })()
;