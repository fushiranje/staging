
jQuery(document).ready(function ($) {
    console.log( "ready!" );
    var windowWidth = $(window).width();
    if(windowWidth<768){
        var cookie = getCookie("smartAds");
        if(cookie == undefined)
        {
            setCookie('smartAds', 0, 1);
            
        }
        var startingClicks = cookie;

        $( "body" ).on( "click", function() {
            cookie = getCookie("smartAds");
            var obj = jQuery.parseJSON( adData );
            var rand = new randomGenerator(0, obj.postCount);
            if(cookie >= 5)
            {
                var randomNum = rand.get();
                setCookie("smartAds",0,1);
                startingClicks = 0;
                cookie = getCookie("smartAds");
                console.log(obj);
                console.log(randomNum);
                $("#myModal").html(obj[randomNum].html);

                jQuery( '#home_slider' ).cycle({ slides: 'img' });

                $("#myModal").modal('show');
            }
            else{
                $("#myModal").modal('hide');
                startingClicks++;
                setCookie('smartAds', startingClicks, 1);
                cookie = getCookie("smartAds");
                // console.log( cookie );
                // console.log( startingClicks );
            }
        });

    }
        
});

function getCookie(cname) 
{
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// setCookie("smartAds",0,1);



// if only one argument is passed, it will assume that is the high
// limit and the low limit will be set to zero
// so you can use either r = new randomeGenerator(9);
// or r = new randomGenerator(0, 9);
function randomGenerator(low, high) {
    if (arguments.length < 2) {
        high = low;
        low = 0;
    }
    this.low = low;
    this.high = high;
    this.reset();
}

randomGenerator.prototype = {
    reset: function() {
        this.remaining = [];
        for (var i = this.low; i <= this.high; i++) {
            this.remaining.push(i);
        }
    },
    get: function() {
        if (!this.remaining.length) {
            this.reset();
        }
        var index = Math.floor(Math.random() * this.remaining.length);
        var val = this.remaining[index];
        this.remaining.splice(index, 1);
        return val;        
    }
}