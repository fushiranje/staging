// Require path.
const path = require( 'path' );

// Configuration object.
const config = {
    entry: './gutenberg/src/block_slider.js',

    // Create the output files.
    // One for each of our entry points.
    output: {
        // [name] allows for the entry object keys to be used as file names.
        filename: 'js/block_slider.js',
        // Specify the path to the JS files.
        path: path.resolve( __dirname, 'assets' )
    },
    devtool: false,
    // Setup a loader to transpile down the latest and great JavaScript so older browsers
    // can understand it.
    module: {
        rules: [
            {
                // Look for any .js files.
                test: /\.js$/,
                // Exclude the node_modules folder.
                exclude: /node_modules/,
                // Use babel loader to transpile the JS files.
                loader: 'babel-loader'
            }
        ]
    }
}

// Export the config object.
module.exports = config;