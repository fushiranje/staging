<?php

function property_gallery_add_metabox(){
	add_meta_box(
		'post_custom_gallery',
		'Gallery',
		'property_gallery_metabox_callback',
		'smartads', // Change post type name
		'normal',
		'core'
	);
}
add_action( 'admin_init', 'property_gallery_add_metabox' );

include(__DIR__.'/style.php');
include(__DIR__.'/script.php');


function property_gallery_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'sample_nonce' ] ) && wp_verify_nonce( $_POST[ 'sample_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	// Correct post type
	if ( !empty($_POST['post_type']) && 'smartads' != $_POST['post_type'] ) // here you can set the post type name
		return;

	if ( !empty($_POST['gallery']) ){

		// Build array for saving post meta
		$gallery_data = array();
		for ($i = 0; $i < count( $_POST['gallery']['image_url'] ); $i++ ){
			if ( '' != $_POST['gallery']['image_url'][$i]){
				$gallery_data['image_url'][]  = $_POST['gallery']['image_url'][ $i ];
			}
		}

		if ( $gallery_data )
			update_post_meta( $post_id, 'gallery_data', $gallery_data );
		else
			delete_post_meta( $post_id, 'gallery_data' );
	}
	// Nothing received, all fields are empty, delete option
	else{
		delete_post_meta( $post_id, 'gallery_data' );
	}
}
add_action( 'save_post', 'property_gallery_save' );