<?php
function property_gallery_mobile_styles_scripts(){
	global $post;
	if( 'smartads' != $post->post_type )
		return;
	?>
	<style type="text/css">
        .gallery_area {
            float:right;
        }
        .image_container {
            float:left!important;
            width: 100px;
            background: url('https://i.hizliresim.com/dOJ6qL.png');
            height: 100px;
            background-repeat: no-repeat;
            background-size: cover;
            border-radius: 3px;
            cursor: pointer;
        }
        .image_container img{
            height: 100px;
            width: 100px;
            border-radius: 3px;
        }
        .clear {
            clear:both;
        }
        #gallery_wrapper {
            width: 100%;
            height: auto;
            position: relative;
            display: inline-block;
        }
        #gallery_wrapper input[type=text] {
            width:300px;
        }
        #gallery_wrapper .gallery_single_row_mobile {
            float: left;
            display:inline-block;
            width: 100px;
            position: relative;
            margin-right: 8px;
            margin-bottom: 20px;
        }
        .dolu {
            display: inline-block!important;
        }
        #gallery_wrapper label {
            padding:0 6px;
        }
        .button.remove {
            background: none;
            color: #f1f1f1;
            position: absolute;
            border: none;
            top: 4px;
            right: 7px;
            font-size: 1.2em;
            padding: 0px;
            box-shadow: none;
        }
        .button.remove:hover {
            background: none;
            color: #fff;
        }
        .button.add {
            background: #c3c2c2;
            color: #ffffff;
            border: none;
            box-shadow: none;
            width: 100px;
            height: 100px;
            line-height: 100px;
            font-size: 4em;
        }
        .button.add:hover, .button.add:focus {
            background: #e2e2e2;
            box-shadow: none;
            color: #0f88c1;
            border: none;
        }
	</style>
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
	<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
	<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script type="text/javascript">
        function remove_img(value) {
            var parent=jQuery(value).parent().parent();
            parent.remove();
        }
        var media_uploader = null;
        function open_media_uploader_image_mobile(obj){
            media_uploader = wp.media({
                frame:    "post",
                state:    "insert",
                multiple: false
            });
            media_uploader.on("insert", function(){
                var json = media_uploader.state().get("selection").first().toJSON();
                var image_url_mobile = json.url;
                var html = '<img class="gallery_img_img_mobile" src="'+image_url_mobile+'" height="55" width="55" onclick="open_media_uploader_image_mobile_this(this)"/>';
                console.log(image_url_mobile);
                console.log(obj);
                jQuery(obj).append(html);
                jQuery(obj).find('.meta_image_url_mobile').val(image_url_mobile);
            });
            media_uploader.open();
        }
        function open_media_uploader_image_mobile_this(obj){
            media_uploader = wp.media({
                frame:    "post",
                state:    "insert",
                multiple: false
            });
            media_uploader.on("insert", function(){
                var json = media_uploader.state().get("selection").first().toJSON();
                var image_url_mobile = json.url;
                console.log(image_url_mobile);
                console.log(obj);
                jQuery(obj).attr('src',image_url_mobile);
                jQuery(obj).siblings('.meta_image_url_mobile').val(image_url_mobile);
            });
            media_uploader.open();
        }

        function open_media_uploader_image_mobile_plus(){
            media_uploader = wp.media({
                frame:    "post",
                state:    "insert",
                multiple: true
            });
            media_uploader.on("insert", function(){

                var length = media_uploader.state().get("selection").length;
                var images = media_uploader.state().get("selection").models

                for(var i = 0; i < length; i++){
                    var image_url_mobile = images[i].changed.url;
                    var box = jQuery('#master_box_mobile').html();
                    jQuery(box).appendTo('#img_box_container_mobile');
                    var element = jQuery('#img_box_container_mobile .gallery_single_row_mobile:last-child').find('.image_container');
                    var html = '<img class="gallery_img_img_mobile" src="'+image_url_mobile+'" height="55" width="55" onclick="open_media_uploader_image_mobile_this(this)"/>';
                    element.append(html);
                    element.find('.meta_image_url_mobile').val(image_url_mobile);
                    console.log(image_url_mobile);
                    console.log(element);
                }
            });
            media_uploader.open();
        }
        jQuery(function() {
            jQuery("#img_box_container_mobile").sortable(); // Activate jQuery UI sortable feature
        });
	</script>
	<?php
}
add_action( 'admin_head-post.php', 'property_gallery_mobile_styles_scripts' );
add_action( 'admin_head-post-new.php', 'property_gallery_mobile_styles_scripts' );
