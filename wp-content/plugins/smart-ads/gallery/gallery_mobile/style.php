<?php

function property_gallery_mobile_metabox_callback(){
	wp_nonce_field( basename(__FILE__), 'sample_nonce_mobile' );
	global $post;
	$gallery_data_mobile = get_post_meta( $post->ID, 'gallery_data_mobile', true );
    // echo "aaaaaaaaaaaaaaaaaaaaaaaaaa";
    // echo var_dump($gallery_data_mobile,true);
    // die();
	?>
	<div id="gallery_wrapper">
		<div id="img_box_container_mobile">
			<?php
			if ( isset( $gallery_data_mobile['image_url_mobile'] ) ){
			for( $i = 0; $i < count( $gallery_data_mobile['image_url_mobile'] ); $i++ ){
			?>
			<div class="gallery_single_row_mobile dolu">
				<div class="gallery_area image_container ">
					<img class="gallery_img_img_mobile" src="<?php esc_html_e( $gallery_data_mobile['image_url_mobile'][$i] ); ?>" height="55" width="55" onclick="open_media_uploader_image_mobile_this(this)"/>
					<input type="hidden"
						   class="meta_image_url"
						   name="gallery_mobile[image_url_mobile][]"
						   value="<?php esc_html_e( $gallery_data_mobile['image_url_mobile'][$i] ); ?>"
					/>
				</div>
				<div class="gallery_area">
					<span class="button remove" onclick="remove_img(this)" title="Remove"/><i class="fas fa-trash-alt"></i></span>
				</div>
				<div class="clear" />
			</div>
		</div>
		<?php
		}
		}
		?>
	</div>
	<div style="display:none" id="master_box_mobile">
		<div class="gallery_single_row_mobile">
			<div class="gallery_area image_container" onclick="open_media_uploader_image_mobile(this)">
				<input class="meta_image_url_mobile" value="" type="hidden" name="gallery_mobile[image_url_mobile][]" />
			</div>
			<div class="gallery_area">
				<span class="button remove" onclick="remove_img(this)" title="Remove"/><i class="fas fa-trash-alt"></i></span>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div id="add_gallery_single_row_mobile">
		<input class="button add" type="button" value="+" onclick="open_media_uploader_image_mobile_plus();" title="Add image"/>
	</div>
	</div>
	<?php
}
