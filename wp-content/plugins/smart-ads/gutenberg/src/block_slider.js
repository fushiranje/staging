
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Placeholder } = wp.components;
const BlockEdit = (props) => {
    return(
        <div className={props.className}>
            <img src="https://fakeimg.pl/1280x350"/>
        </div>
    );
}

registerBlockType('awp/slider', {
    title: __('AWP Slider', 'awp'),
    icon: 'slides',
    category: 'embed',
    supports: {
        align: ['center', 'wide', 'full']
    },
    attributes: {
        adBlockId: {
            type: 'number',
            default: 0
        }
    },
    edit: BlockEdit,
    save: () => { return null; }
});
