<?php


	// get_option('dbi_example_plugin_options')
	add_filter( 'the_content', 'prefix_insert_post_ads' );

	function prefix_insert_post_ads( $content ) {

		
	
		if ( is_single() && ! is_admin() ) {
			return prefix_insert_after_paragraph( 5, $content);
		}
	
		return $content;
	}
	

	function prefix_insert_after_paragraph($adOccurence, $content) 
	{
		$closing_p = '</p>';
		$paragraphs = explode( $closing_p, $content );
		$paragraphCount = count($paragraphs);

		$args = array(
			'post_type' => 'smartads',
			'posts_per_page' => -1,
			'post_status' => 'publish',
		);

		$postQuery = new WP_Query($args);
		$posts = $postQuery->get_posts();
		$postsCount = !empty($posts)?count($posts)-1:0;
		
        

		$i = 0;
        $exclude = array();
		
        foreach ($paragraphs as $index => $paragraph) {
            
            

            // $random_number = randWithout(0,$postsCount, $exclude);

			if ( trim( $paragraph ) ) {
				$paragraphs[$index] .= $closing_p;
			}

			if ( ($index + 1) % $adOccurence == 0 && $i <= $postsCount ) {
                
                $random_number = randWithout(0,$postsCount, $exclude);
                if (count($exclude) >= $postsCount-1)
                {
                    $exclude = array();
                }
                $exclude[$random_number] = $random_number;
				$testRender = '';
				$testRender .= '<div class="entry-content-wrap">
									<div class="entry-content">';
									$testRender .= postSliderRender($posts,$random_number);
					$testRender .= '</div>';
				$testRender .= '</div>';
			

				$paragraphs[$index] .= $testRender;
				$i++;
			}
		}

		return implode( '', $paragraphs );
	
		return $content;
	}


	function postSliderRender($posts, $random_number) {

		$postsCount = !empty($posts)?count($posts)-1:0;
		
		$ad = !empty($posts[$random_number])?$posts[$random_number]:array();

		if (!empty($ad))
		{
            $options = get_option( 'dbi_smart_ads_options' );
			
            $cycleTimeout = !empty($options["slider_seconds"])?intval($options["slider_seconds"]) * 1000:4000;

            $ad_url = get_post_meta( $ad->ID, 'ad_url', true );
			$ad_meta = get_post_meta( $ad->ID, 'gallery_data', true );

			$output = "";
			if (!empty($ad_meta["image_url"]))
			{
						// $output .= "<h1>".$random_number."</h1>";
                        $output .= '<a href="'.$ad_url.'" target="_blank">';
                            $output .= '<div class="wp-block-awp-slider aligncenter" style="width:100%; height=300px;">';
                                $output .= '<div class="cycle-slideshow" data-cycle-timeout='.$cycleTimeout.'>';
                                foreach($ad_meta["image_url"] as $url)
                                {
                                    $output .= '<img src="' . $url . '" style="width:100%; height=300px;"/>';
                                }
                                $output .= '</div>';
                            $output .= '</div>';
                        $output .= '<a/>';
                //     $output .= '</div>';
                // $output .= '</div>';
			}

			return $output;

		} else {
			return '';
		}
	}