<?php
/**
 * Register layouts and sections for the Layout block.
 *
 * @package AtomicBlocks
 */

namespace AtomicBlocks\Layouts;

add_action( 'plugins_loaded', __NAMESPACE__ . '\register_components', 11 );
/**
 * Registers section and layout components.
 *
 * @since 2.0
 */
function register_components() {
	blockspare_register_layout_component(
		[
			'type'     => 'layout',
			'key'      => 'bs_layout_business_1',
			'name'     => esc_html__( 'Accordion', 'blockspare' ),
			'content'  => "<!-- wp:blockspare/blockspare-accordion {\"uniqueId\":\"488dbf\"} -->
<div class=\"blockspare-block-accordion blockspare-block blockspare-block-488dbf\" data-item-toggle=\"true\"><!-- wp:blockspare/accordion-item {\"uniqueId\":\"0e3899\",\"itemNumber\":0,\"defaultText\":\"Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.\"} -->
<div class=\"wp-block-blockspare-accordion-item blockspare-block-0e3899\"><div class=\"blockspare-accordion-item blockspare-type-fill \" data-act-color=\"#fff\" data-txt-color=\"#fff\" data-active=\"#8b249c\" data-pan=\"#3c1a5b\" style=\"margin-bottom:10px\"><div class=\"blockspare-accordion-panel  blockspare-right\" style=\"background-color:#3c1a5b\"><span class=\"blockspare-accordion-panel-handler\" role=\"button\" style=\"color:#fff\"><span class=\"blockspare-accordion-icon fa fa-plus\"></span><span class=\"blockspare-accordion-panel-handler-label\">Accordion Item</span></span></div><div class=\"blockspare-accordion-body\" style=\"background-color:#fff\" data-bg=\"#fff\"><!-- wp:paragraph {\"customFontSize\":18} -->
<p style=\"font-size:18px\">Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.</p>
<!-- /wp:paragraph --></div></div></div>
<!-- /wp:blockspare/accordion-item -->

<!-- wp:blockspare/accordion-item {\"uniqueId\":\"0ee545\",\"itemNumber\":1,\"defaultText\":\"Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.\"} -->
<div class=\"wp-block-blockspare-accordion-item blockspare-block-0ee545\"><div class=\"blockspare-accordion-item blockspare-type-fill \" data-act-color=\"#fff\" data-txt-color=\"#fff\" data-active=\"#8b249c\" data-pan=\"#3c1a5b\" style=\"margin-bottom:10px\"><div class=\"blockspare-accordion-panel  blockspare-right\" style=\"background-color:#3c1a5b\"><span class=\"blockspare-accordion-panel-handler\" role=\"button\" style=\"color:#fff\"><span class=\"blockspare-accordion-icon fa fa-plus\"></span><span class=\"blockspare-accordion-panel-handler-label\">Accordion Item</span></span></div><div class=\"blockspare-accordion-body\" style=\"background-color:#fff\" data-bg=\"#fff\"><!-- wp:paragraph {\"customFontSize\":18} -->
<p style=\"font-size:18px\">Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.</p>
<!-- /wp:paragraph --></div></div></div>
<!-- /wp:blockspare/accordion-item -->

<!-- wp:blockspare/accordion-item {\"uniqueId\":\"b8b5d4\",\"itemNumber\":2,\"defaultText\":\"Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.\"} -->
<div class=\"wp-block-blockspare-accordion-item blockspare-block-b8b5d4\"><div class=\"blockspare-accordion-item blockspare-type-fill \" data-act-color=\"#fff\" data-txt-color=\"#fff\" data-active=\"#8b249c\" data-pan=\"#3c1a5b\" style=\"margin-bottom:10px\"><div class=\"blockspare-accordion-panel  blockspare-right\" style=\"background-color:#3c1a5b\"><span class=\"blockspare-accordion-panel-handler\" role=\"button\" style=\"color:#fff\"><span class=\"blockspare-accordion-icon fa fa-plus\"></span><span class=\"blockspare-accordion-panel-handler-label\">Accordion Item</span></span></div><div class=\"blockspare-accordion-body\" style=\"background-color:#fff\" data-bg=\"#fff\"><!-- wp:paragraph {\"customFontSize\":18} -->
<p style=\"font-size:18px\">Ipsam per dolores minus natoque? Rutrum dolorem voluptates euismod pharetra! Rhoncus distinctio cupiditate accusantium. Cillum aliquid.</p>
<!-- /wp:paragraph --></div></div></div>
<!-- /wp:blockspare/accordion-item --></div>
<!-- /wp:blockspare/blockspare-accordion -->",
			'category' => [ esc_html__( 'business', 'blockspare' ) ],
			'keywords' => [
				esc_html__( 'business', 'blockspare' ),
				esc_html__( 'service', 'blockspare' ),
				esc_html__( 'product', 'blockspare' ),
			],
			'image'    => 'https://demo.studiopress.com/page-builder/ab-layout-business-1.jpg',
		]
	);

	
	blockspare_register_layout_component(
		[
			'type'     => 'section',
			'key'      => 'bs_section_container',
			'content'  => "<!-- wp:blockspare/blockspare-container {\"imgURL\":null,\"imgID\":null,\"imgAlt\":null,\"backGroundColor\":\"#cd2653\",\"separatorEnable\":true,\"enableGradient\":true,\"backgroundColor1\":\"#cd2653\",\"design\":\"fan\",\"color\":\"#dcd7ca\"} -->
<div class=\"wp-block-blockspare-blockspare-container alignfull blockspare-block-container-wrapper has-separator-top\" style=\"background-image:linear-gradient(-90deg,#cd2653 30%,#8B249C 70%);min-height:100px;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;margin-top:30px;margin-bottom:30px\"><div class=\"blockspare-container-background blockspare-image-wrap has-background-opacity-100 has-background-opacity\" style=\"background-image:\"></div><div class=\"blockspare-top-separator\" style=\"top:0px;z-index:\"><div class=\"blockspare-blocks blockspare-svg-wrapper has-width-1 is-vertically-flipped\" style=\"color:#dcd7ca;height:150px\"><div class=\"blockspare-svg-svg-inner blockspare-separator-wrapper\"><svg class=\"fan\" preserveaspectratio=\"none\" aria-hidden=\"true\" xmlns=\"http://www.w3.org/2000/svg\" viewbox=\"0 0 1917 105\"><path class=\"st1\" d=\"M-.5 0l958.8 69.3L1917 0v77H-.5z\"></path><path class=\"st2\" d=\"M-.5 24.5l958.8 46.3L1917 24.5V77H-.5z\"></path><path class=\"st3\" d=\"M-.5 49.1l958.8 23.1L1917 49.1V77H-.5z\"></path><path d=\"M-.5 77H1917v28H-.5z\"></path></svg></div></div></div><div class=\"blockspare-container\"><div class=\"blockspare-inner-blocks blockspare-inner-wrapper-blocks\"><!-- wp:blockspare/content-box {\"headertitleColor\":\"#f5efe0\",\"headersubtitleColor\":\"#f5efe0\",\"dashColor\":\"#f5efe0\",\"imgURL\":null,\"imgID\":null,\"backGroundColor\":\"#cd2653\",\"descriptionColor\":\"#dcd7ca\",\"enableBoxShadow\":true} -->
<div class=\"aligncenter blockspare-content-wrapper blockspare-blocks aligncenter has-background style-1\" style=\"text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;margin-top:30px;margin-bottom:30px\"><div class=\"empty-plcaeholder-div\"></div><div class=\"blockspare-section-wrapper\" style=\"box-shadow:0px 6px 12px -10px #000;background-color:#cd2653\"><div class=\"blockspare-section-header-wrapper\" style=\"border-color:#f5efe0\"><div class=\"blockspare-section-head-wrap blockspare-style1 blockspare-center\"><div class=\"blockspare-title-subtitle-wrap\"><div class=\"blockspare-section-header-wrapper blockspare-blocks aligncenter\" style=\"text-align:center;padding-top:0px;padding-right:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px;padding-left:0px;background-color:transparent\"><div class=\"blockspare-section-head-wrap blockspare-style1 blockspare-center\"><div class=\"blockspare-title-wrapper\"><span class=\"blockspare-title-dash blockspare-upper-dash\" style=\"border-color:#f5efe0\"></span><h2 class=\"blockspare-title\" style=\"color:#f5efe0;font-size:32px;padding-top:5px;padding-bottom:5px\">Enter Title</h2><span class=\"blockspare-title-dash blockspare-lower-dash\" style=\"border-color:#f5efe0\"></span></div><div class=\"blockspare-subtitle-wrapper\"><span class=\"blockspare-title-dash blockspare-upper-dash\" style=\"border-color:#f5efe0\"></span><p class=\"blockspare-subtitle\" style=\"font-size:18px;color:#f5efe0;padding-top:5px;padding-bottom:5px\">Enter Subtitle</p><span class=\"blockspare-title-dash blockspare-lower-dash\" style=\"border-color:#f5efe0\"></span></div></div></div></div></div><div class=\"blockspare-desc-btn-wrap\"><p style=\"color:#dcd7ca\">Add your description here...</p></div></div></div></div>
<!-- /wp:blockspare/content-box --></div></div></div>
<!-- /wp:blockspare/blockspare-container -->",
			'name'     => esc_html__( 'Container', 'blockspare' ),
			'category' => [ esc_html__( 'container', 'blockspare' ) ],
			'keywords' => [
				esc_html__( 'contact', 'blockspare' ),
				esc_html__( 'map', 'blockspare' ),
				esc_html__( 'email', 'blockspare' ),
				esc_html__( 'directions', 'blockspare' ),
			],
			'image'    => 'https://demo.studiopress.com/page-builder/ab_layout_contact.jpg',
		]
	);
}
