<?php
if (!class_exists('Newsphere_Trending_Posts_Carousel')) :
    /**
     * Adds Newsphere_Trending_Posts_Carousel widget.
     */
    class Newsphere_Trending_Posts_Carousel extends AFthemes_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 1.0.0
         */
        function __construct()
        {
            $this->text_fields = array('newsphere-posts-slider-title', 'newsphere-posts-slider-subtitle', 'newsphere-posts-slider-number');
            $this->select_fields = array('newsphere-select-category');

            $widget_ops = array(
                'classname' => 'newsphere_trending_posts_carousel_widget grid-layout',
                'description' => __('Displays posts carousel from selected category.', 'newsphere'),

            );

            parent::__construct('newsphere_trending_posts_carousel', __('AFTN Trending Posts Carousel', 'newsphere'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {
            $instance = parent::newsphere_sanitize_data($instance, $instance);
            /** This filter is documented in wp-includes/default-widgets.php */

            $title = apply_filters('widget_title', $instance['newsphere-posts-slider-title'], $instance, $this->id_base);

            $number_of_posts = isset($instance['newsphere-posts-slider-number']) ? $instance['newsphere-posts-slider-number'] : 7;
            $category = isset($instance['newsphere-select-category']) ? $instance['newsphere-select-category'] : '0';

            // open the widget container
            echo $args['before_widget'];
            ?>
            <?php if (!empty($title)): ?>
            <div class="em-title-subtitle-wrap">
                <?php if (!empty($title)): ?>
                    <h4 class="widget-title header-after1">
                        <span class="header-after">
                            <?php echo esc_html($title);  ?>
                            </span>
                    </h4>
                <?php endif; ?>
            </div>
        <?php endif; ?>
            <?php
            $newsphere_nav_control_class = empty($title) ? 'no-section-title' : '';
            $all_posts = newsphere_get_posts($number_of_posts, $category);
            $count = 1;
            ?>
            <div class="trending-posts-vertical af-widget-carousel swiper-container">
                <div class="swiper-wrapper">
                    <?php
                    if ($all_posts->have_posts()) :
                        while ($all_posts->have_posts()) : $all_posts->the_post();

                            $aft_post_id = get_the_ID();
                            $url = newsphere_get_freatured_image_url($aft_post_id, 'thumbnail');

                            ?>
                            <div class="swiper-slide">
                                <div class="af-double-column list-style clearfix">
                                    <div class="read-single color-pad">
                                        <div class="data-bg read-img pos-rel col-4 float-l read-bg-img" data-background="<?php echo esc_url($url); ?>">
                                            <img src="<?php echo esc_url($url); ?>">
                                            <a href="<?php the_permalink(); ?>"></a>
                                            <span class="trending-no">
                                                <?php echo sprintf( __( '%s', 'newsphere' ), $count); ?>
                                            </span>
                                            <span class="min-read-post-format">
                                                <?php newsphere_count_content_words($aft_post_id); ?>
                                            </span>
                                        </div>

                                        <div class="read-details col-75 float-l pad color-tp-pad">
                                            <div class="read-categories">
                                                <?php newsphere_post_categories(); ?>
                                            </div>
                                            <div class="read-title">
                                                <h4>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php the_title(); ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="entry-meta">
                                                <?php newsphere_get_comments_count($aft_post_id); ?>
                                                <?php newsphere_post_item_meta(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $count++;
                        endwhile;
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>

            <?php
            //print_pre($all_posts);

            // close the widget container
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            $categories = newsphere_get_terms();
            if (isset($categories) && !empty($categories)) {
                // generate the text input for the title of the widget. Note that the first parameter matches text_fields array entry
                echo parent::newsphere_generate_text_input('newsphere-posts-slider-title', 'Title', 'Trending Posts Carousel');
                echo parent::newsphere_generate_select_options('newsphere-select-category', __('Select category', 'newsphere'), $categories);
                echo parent::newsphere_generate_text_input('newsphere-posts-slider-number', __('Number of posts', 'newsphere'), '7');


            }
        }
    }
endif;