<?php

/**
 * Adds Newsphere_Video_Slider widget.
 */
class Newsphere_Video_Slider extends AFthemes_Widget_Base
{
    /**
     * Sets up a new widget instance.
     *
     * @since 1.0.0
     */
    function __construct()
    {
        $this->text_fields = array('newsphere-youtube-video-slider-title');

        $this->url_fields = array('newsphere-youtube-video-url-1', 'newsphere-youtube-video-url-2', 'newsphere-youtube-video-url-3', 'newsphere-youtube-video-url-4', 'newsphere-youtube-video-url-5', 'newsphere-youtube-video-url-6', 'newsphere-youtube-video-url-7', 'newsphere-youtube-video-url-8', 'newsphere-youtube-video-url-9', 'newsphere-youtube-video-url-10');


        $widget_ops = array(
            'classname' => 'newsphere_youtube_video_slider_widget',
            'description' => __('Displays youtube video slider.', 'newsphere'),

        );

        parent::__construct('newsphere_video_slider', __('AFTN YouTube Video Slider', 'newsphere'), $widget_ops);
    }

    /**
     * Outputs the content for the current widget instance.
     *
     * @since 1.0.0
     *
     * @param array $args Display arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        $instance = parent::newsphere_sanitize_data($instance, $instance);
        $title = apply_filters('widget_title', $instance['newsphere-youtube-video-slider-title'], $instance, $this->id_base);
        echo $args['before_widget'];

        ?>
        <?php if (!empty($title)): ?>
        <div class="em-title-subtitle-wrap">
            <?php if (!empty($title)): ?>
                <h4 class="widget-title header-after1">
                    <span class="header-after"><?php echo esc_html($title); ?></span>
                </h4>
            <?php endif; ?>
        </div>
    <?php endif; ?>
        <?php if (!empty($instance['newsphere-youtube-video-url-1'])): ?>
        <div class="slider-pro">

		    <?php

		    $mp_video_url_1 = $instance['newsphere-youtube-video-url-1'];
		    parse_str(parse_url($mp_video_url_1, PHP_URL_QUERY), $my_array_of_vars_1);
		    if($mp_video_url_1):
			    ?>
                <container class="vid-main-wrapper clearfix">
                    <!-- THE YOUTUBE PLAYER -->
                    <div class="vid-container af-video-wrap" data-video-link="">
                        <span class="af-bg-play af-hide-iframe">
                            <i class="fa fa-play" aria-hidden="true"></i>
                        </span>    
                        <iframe id="vid_frame" class="af-hide-iframe" frameborder="0"   allow="autoplay" allowfullscreen></iframe>
                    </div>
                </container>
		    <?php endif; ?>
            <div class="swiper-container swiper-container-videos af-video-slider af-widget-carousel">
                <ol class="swiper-wrapper" style="list-style-type: none; padding: 0px;">
				    <?php for ($i = 1; $i <= 7; $i++) {
				        if($i==1){
				            $first_image_class =  'first_thb_img';
                        }else{
					        $first_image_class =  '';
                        }
				        ?>

					    <?php $mp_video_url = $instance['newsphere-youtube-video-url-' . $i]; ?>
					    <?php if (!empty($mp_video_url)) { ?>
						    <?php
						    $url = $mp_video_url;
						    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
						    $yt_item = $my_array_of_vars['v'];
						    $max_full_url = newsphere_youtube_thumbnail_img($yt_item);
						    $link = 'https://www.youtube.com/embed/'.$my_array_of_vars["v"].'?autoplay=1&rel=0&showinfo=0&autohide=1" allow="autoplay"  allowfullscreen';
						    ?>
                            <li class="swiper-slide">
                                <a class="af-custom-thumbnail <?php echo $first_image_class; ?>" href="javascript:void(0)" data-item="<?php echo $max_full_url;?>"
                                   data-video="<?php echo $link;?>">
          <span class="vid-thumb">
     <img src="https://img.youtube.com/vi/<?php echo $my_array_of_vars['v']; ?>/mqdefault.jpg"/>
        </span>

                                </a>
                            </li>

					    <?php } else {
						    //_e('Video URL not found','newsphere' );
					    } ?>

				    <?php } ?>
                </ol>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- If we need navigation buttons -->
                <div class="swiper-button-next af-slider-btn swiper-custom-next"></div>
                <div class="swiper-button-prev af-slider-btn swiper-custom-prev"></div>
            </div>
        </div>
    <?php endif; ?>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @since 1.0.0
     *
     * @param array $instance Previously saved values from database.
     *
     *
     */
    public function form($instance)
    {
        $this->form_instance = $instance;

        // generate the text input for the title of the widget. Note that the first parameter matches text_fields array entry
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-slider-title', 'Title', 'YouTube Video Slider');

        ?><h4><?php _e('YouTube links:', 'newsphere'); ?></h4>
        <?php
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-1', 'Video 1', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-2', 'Video 2', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-3', 'Video 3', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-4', 'Video 4', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-5', 'Video 5', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-6', 'Video 6', '');
        echo parent::newsphere_generate_text_input('newsphere-youtube-video-url-7', 'Video 7', '');


    }

}