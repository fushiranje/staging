<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


/**
 * Customizer
 *
 * @class   newsphere
 */

if (!function_exists('newsphere_custom_style')) {

    function newsphere_custom_style()
    {

        global $newsphere_google_fonts;

        $primary_color = newsphere_get_option('primary_color');
        $secondary_color = newsphere_get_option('secondary_color');
        $text_over_secondary_color = newsphere_get_option('text_over_secondary_color');
        $link_color = newsphere_get_option('link_color');
        $site_wide_title_color = newsphere_get_option('site_wide_title_color');
        $top_header_background_color = newsphere_get_option('top_header_background_color');
        $top_header_text_color = newsphere_get_option('top_header_text_color');
        $main_navigation_link_color = newsphere_get_option('main_navigation_link_color');
        $main_navigation_custom_background_color = newsphere_get_option('main_navigation_custom_background_color');
        $main_navigation_badge_background = newsphere_get_option('main_navigation_badge_background');
        $main_navigation_badge_color = newsphere_get_option('main_navigation_badge_color');
        $title_link_color = newsphere_get_option('title_link_color');
        $title_over_image_color = newsphere_get_option('title_over_image_color');
        $post_format_color = newsphere_get_option('post_format_color');
        $site_default_post_box_color = newsphere_get_option('site_default_post_box_color');
        $footer_mailchimp_background_color = newsphere_get_option('footer_mailchimp_background_color');
        $footer_mailchimp_text_color = newsphere_get_option('footer_mailchimp_text_color');
        $footer_background_color = newsphere_get_option('footer_background_color');
        $footer_texts_color = newsphere_get_option('footer_texts_color');
        $footer_credits_background_color = newsphere_get_option('footer_credits_background_color');
        $footer_credits_texts_color = newsphere_get_option('footer_credits_texts_color');
        $category_color_1 = newsphere_get_option('category_color_1');
        $category_color_2 = newsphere_get_option('category_color_2');
        $category_color_3 = newsphere_get_option('category_color_3');
        $category_color_4 = newsphere_get_option('category_color_4');
        $category_color_5 = newsphere_get_option('category_color_5');
        $category_color_6 = newsphere_get_option('category_color_6');
        $category_color_7 = newsphere_get_option('category_color_7');


        
        $primary_font = $newsphere_google_fonts[newsphere_get_option('primary_font')];
        $secondary_font = $newsphere_google_fonts[newsphere_get_option('secondary_font')];

        $general_post_title_size = newsphere_get_option('general_post_title_size');
        $spotlight_post_title_size = newsphere_get_option('spotlight_post_title_size');
        $spotlight_post_title_over_image_size = newsphere_get_option('spotlight_post_title_over_image_size');
        $section_title_font_size = newsphere_get_option('section_title_font_size');
        $single_posts_title_size = newsphere_get_option('single_posts_title_size');


        $letter_spacing = newsphere_get_option('letter_spacing');
        $line_height = newsphere_get_option('line_height');


        $main_banner_section_background_color = newsphere_get_option('main_banner_section_background_color');

        $main_banner_section_secondary_background_color = newsphere_get_option('main_banner_section_secondary_background_color');
        $main_banner_section_texts_color = newsphere_get_option('main_banner_section_texts_color');


        ob_start();
        ?>

    <?php if (!empty($primary_color)): ?>
        body.aft-default-mode blockquote:before,
        body.aft-default-mode.single-post:not(.aft-single-full-header) .entry-header span.min-read-post-format .af-post-format i,
        body.aft-default-mode .main-navigation ul.children li a, 
        body.aft-default-mode .main-navigation ul .sub-menu li a,
        body.aft-default-mode .read-details .entry-meta span a,
        body.aft-default-mode .read-details .entry-meta span,
        body.aft-default-mode h4.af-author-display-name,
        body.aft-default-mode #wp-calendar caption,
        body.aft-default-mode ul.trail-items li a,
        body.aft-default-mode {
        color: <?php echo $primary_color; ?>;
        }

        body.aft-default-mode.single-post:not(.aft-single-full-header) .entry-header span.min-read-post-format .af-post-format i:after{
        border-color: <?php echo $primary_color; ?>;
        }

    <?php endif; ?>      

    <?php if (!empty($secondary_color)): ?>

        body.aft-default-mode .inner-suscribe input[type=submit],
        body.aft-default-mode button:not(.offcanvas-nav):not(.toggle-menu),
        body.aft-default-mode input[type="button"],
        body.aft-default-mode input[type="reset"],
        body.aft-default-mode input[type="submit"],
        body.aft-default-mode #scroll-up,
        body.aft-default-mode .trending-posts-vertical .trending-no,
        body.aft-default-mode .aft-trending-latest-popular .nav-tabs>li>a:focus-within,
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li a.active,
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li a:hover,
        body.aft-default-mode .aft-main-banner-wrapper span.trending-no,
        body.aft-default-mode .read-img .min-read-post-comment,
        body.aft-default-mode .aft-home-icon {
        background-color: <?php echo $secondary_color; ?>;
        }

        body.aft-default-mode .read-img .min-read-post-comment:after{
        border-top-color: <?php echo $secondary_color; ?>;
        }
        
        
        bodybody.aft-default-mode  .sticky .read-title h4 a:before {
        color: <?php echo $secondary_color; ?>;
        }


        body.aft-default-mode .newsphere_tabbed_posts_widget .nav-tabs > li > a:hover,
        body.aft-default-mode .newsphere_tabbed_posts_widget .nav-tabs > li > a,
        body.aft-default-mode .related-title,
        body.aft-default-mode .widget-title span, body.aft-default-mode .header-after1 span{
        border-bottom-color: <?php echo $secondary_color; ?>;
        }

        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-1,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-1,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-2,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-2,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-3,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-3,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-4,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-4,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-5,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-5,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-6,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-6,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-7,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-7,
        body.aft-default-mode .af-sp-wave:before, body.aft-default-mode .af-sp-wave:after ,
        body.aft-default-mode .af-video-slider .swiper-slide.selected{
        border-color: <?php echo $secondary_color; ?>;
        }
    <?php endif; ?>  

    <?php if (!empty($text_over_secondary_color)): ?>
        body.aft-default-mode button:not(.offcanvas-nav):not(.toggle-menu),
        body.aft-default-mode input[type="button"],
        body.aft-default-mode input[type="reset"],
        body.aft-default-mode input[type="submit"],
        body.aft-default-mode .inner-suscribe input[type=submit],
        body.aft-default-mode #scroll-up,
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li>a.active:hover, 
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li>a:hover, 
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li>a.active,
        body.aft-default-mode .read-img .min-read-post-comment a,
        body.aft-default-mode .aft-home-icon a,
        body.aft-default-mode .aft-main-banner-wrapper span.trending-no,
        body.aft-default-mode .trending-posts-vertical .trending-no{
        color: <?php echo $text_over_secondary_color; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($top_header_background_color)): ?>
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header {
        background-color: <?php echo $top_header_background_color; ?>;
        }
    <?php endif; ?>  
    <?php if (!empty($top_header_text_color)): ?>
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .date-bar-left{
        color: <?php echo $top_header_text_color; ?>;
        }
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .offcanvas-menu span{
        background-color: <?php echo $top_header_text_color; ?>;
        }
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .offcanvas:hover .offcanvas-menu span.mbtn-bot,
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .offcanvas:hover .offcanvas-menu span.mbtn-top,
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .offcanvas-menu span.mbtn-top ,
        body.aft-default-mode .header-style1:not(.header-layout-2) .top-header .offcanvas-menu span.mbtn-bot{
        border-color: <?php echo $top_header_text_color; ?>;
        }
    <?php endif; ?>  

    <?php if (!empty($link_color)): ?>
        body.aft-default-mode .widget_text a,
        body.aft-default-mode footer .widget_text a,
        body.aft-default-mode #sidr .widget_text a,
        body.aft-default-mode .author-links a,
        body.aft-default-mode .entry-content > [class*="wp-block-"] a:not(.has-text-color),
        body.aft-default-mode .entry-content > ul a,
        body.aft-default-mode .entry-content > ol a,
        body.aft-default-mode .entry-content > p a {
        color: <?php echo $link_color; ?>;
        }
    <?php endif; ?>   

    <?php if (!empty($main_navigation_link_color)): ?>
        body.aft-default-mode .main-navigation ul li a,
        body.aft-default-mode .search-icon:visited, 
        body.aft-default-mode .search-icon:hover, 
        body.aft-default-mode .search-icon:focus, 
        body.aft-default-mode .search-icon:active,
        body.aft-default-mode .search-icon{ 
        color: <?php echo $main_navigation_link_color; ?>;
        }
        body.aft-default-mode .ham:before, 
        body.aft-default-mode .ham:after,
        body.aft-default-mode .ham{
        background-color: <?php echo $main_navigation_link_color; ?>;
        }

        
    <?php endif; ?>    

    <?php if (!empty($main_navigation_custom_background_color)): ?>
        body.aft-default-mode .bottom-bar {
        background-color: <?php echo $main_navigation_custom_background_color; ?>;
        }
    <?php endif; ?>     


    <?php if (!empty($main_navigation_badge_background)): ?>
        body.aft-default-mode .main-navigation .menu-description {
        background-color: <?php echo $main_navigation_badge_background; ?>;
        }
        body.aft-default-mode .main-navigation .menu-description:after{
        border-top-color: <?php echo $main_navigation_badge_background; ?>;
        }
    <?php endif; ?> 
    
    <?php if (!empty($main_navigation_badge_color)): ?>
        body.aft-default-mode .main-navigation .menu-description {
        color: <?php echo $main_navigation_badge_color; ?>;

        }

    <?php endif; ?>    

    <?php if (!empty($main_banner_section_background_color)): ?>
        body.aft-default-mode.single-post.aft-single-full-header header.entry-header.pos-rel.aft-no-featured-image ,
        body.aft-default-mode .aft-blocks.banner-carousel-1-wrap {
        background-color: <?php echo $main_banner_section_background_color; ?>;

        }

    <?php endif; ?>



    <?php if (!empty($main_banner_section_secondary_background_color)): ?>
        
        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs li,
        body.aft-default-mode.aft-dark-mode .aft-main-banner-section .read-single.color-pad,
        body.aft-default-mode .aft-main-banner-section .read-single.color-pad {
        background-color: <?php echo $main_banner_section_secondary_background_color; ?>;
        }

        body.aft-default-mode.aft-transparent-main-banner-box .aft-main-banner-section .aft-trending-latest-popular li.af-double-column.list-style:before,
        body.aft-default-mode.aft-transparent-main-banner-box .aft-main-banner-section .read-single.color-pad:before {
            background-color: <?php echo $main_banner_section_secondary_background_color; ?>;
        }

    <?php endif; ?>

    <?php if (!empty($main_banner_section_texts_color)): ?>
        

        body.aft-default-mode .aft-main-banner-section .aft-trending-latest-popular .nav-tabs>li>a,
        body.aft-default-mode.aft-dark-mode .aft-main-banner-section .color-pad,
        body.aft-default-mode.aft-dark-mode .aft-main-banner-section .color-pad .entry-meta span a,
        body.aft-default-mode.aft-dark-mode .aft-main-banner-section .color-pad .entry-meta span,
        body.aft-default-mode.aft-dark-mode .aft-main-banner-section .color-pad .read-title h4 a,
        body.aft-default-mode .aft-main-banner-section .color-pad,
        body.aft-default-mode .aft-main-banner-section .color-pad .entry-meta span a,
        body.aft-default-mode .aft-main-banner-section .color-pad .entry-meta span,
        body.aft-default-mode .aft-main-banner-section .color-pad .read-title h4 a{

        color: <?php echo $main_banner_section_texts_color; ?>;

        }

    <?php endif; ?>

    <?php if (!empty($site_wide_title_color)): ?>
        body.aft-default-mode .page-title,
        body.aft-default-mode .newsphere_tabbed_posts_widget .nav-tabs > li > a,
        body.aft-default-mode h1.entry-title,
        body.aft-default-mode .widget-title,body.aft-default-mode  .header-after1 {
        color: <?php echo $site_wide_title_color; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($title_link_color)): ?>
        body.aft-default-mode .read-title h4 a {
        color: <?php echo $title_link_color; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($title_over_image_color)): ?>
        body.aft-default-mode.single-post.aft-single-full-header .entry-header .read-details, body.aft-default-mode.single-post.aft-single-full-header .entry-header .entry-meta span a, body.aft-default-mode.single-post.aft-single-full-header .entry-header .entry-meta span, body.aft-default-mode.single-post.aft-single-full-header .entry-header .read-details .entry-title,
        body.aft-default-mode.single-post.aft-single-full-header .entry-header .cat-links li a, body.aft-default-mode.single-post.aft-single-full-header .entry-header .entry-meta span a, body.aft-default-mode.single-post.aft-single-full-header .entry-header .entry-meta span, body.aft-default-mode.single-post.aft-single-full-header .entry-header .read-details .entry-title,
        body.aft-default-mode .af-main-banner-image-active .main-banner-widget-section .header-after1 span,
        body.aft-default-mode .site-footer .color-pad .big-grid .read-title h4 a ,
        body.aft-default-mode .big-grid .read-details .entry-meta span, 
        body.aft-default-mode .big-grid .read-details .entry-meta span a, 
        body.aft-default-mode .big-grid .read-title h4 a {
        color: <?php echo $title_over_image_color; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($post_format_color)): ?>
        body.aft-default-mode.single-post.aft-single-full-header .entry-header span.min-read-post-format .af-post-format i,
        body.aft-default-mode .af-bg-play i,
        body.aft-default-mode .af-post-format i {
        color: <?php echo $post_format_color; ?>;
        }
        body.aft-default-mode.single-post.aft-single-full-header .entry-header span.min-read-post-format .af-post-format i:after,
        body.aft-default-mode .af-bg-play i:after,
        body.aft-default-mode .af-post-format i:after{
        border-color: <?php echo $post_format_color; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($site_default_post_box_color)): ?>
        body.aft-default-mode blockquote:before,
        body.aft-default-mode .sidr, 
        body.aft-default-mode #comments.comments-area, 
        body.aft-default-mode .read-single .color-pad, 
        body.aft-default-mode.single-content-mode-boxed article.af-single-article, 
        body.aft-default-mode #secondary .widget-area.color-pad .widget, 
        body.aft-default-mode .read-single.color-pad {
        background-color: <?php echo $site_default_post_box_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_mailchimp_text_color)): ?>
        body.aft-default-mode .mailchimp-block .block-title{
        color: <?php echo $footer_mailchimp_text_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_mailchimp_background_color)): ?>
        body.aft-default-mode .mailchimp-block{
        background-color: <?php echo $footer_mailchimp_background_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_background_color)): ?>
        body.aft-default-mode footer.site-footer{
        background-color: <?php echo $footer_background_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_texts_color)): ?>
        body.aft-default-mode .site-footer h4.af-author-display-name,
        body.aft-default-mode .site-footer .newsphere_tabbed_posts_widget .nav-tabs > li > a,
        body.aft-default-mode .site-footer .color-pad .entry-meta span a,
        body.aft-default-mode .site-footer .color-pad .entry-meta span,
        body.aft-default-mode .site-footer .color-pad .read-title h4 a,
        body.aft-default-mode .site-footer #wp-calendar caption,
        body.aft-default-mode .site-footer .header-after1 span,
        body.aft-default-mode .site-footer .widget-title span, 
        body.aft-default-mode .site-footer .widget ul li,
        body.aft-default-mode .site-footer .color-pad ,
        body.aft-default-mode .site-footer a,
        body.aft-default-mode .site-footer ,
        body.aft-default-mode footer.site-footer{
        color: <?php echo $footer_texts_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_credits_background_color)): ?>
        body.aft-default-mode .site-info{
        background-color: <?php echo $footer_credits_background_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($footer_credits_texts_color)): ?>
        body.aft-default-mode .site-info .color-pad a,
        body.aft-default-mode .site-info .color-pad{
        color: <?php echo $footer_credits_texts_color; ?>;
        }
    <?php endif; ?> 

    <?php if (!empty($category_color_1)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-1 {
        border-color: <?php echo $category_color_1; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-1 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-1,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-1{
        color: <?php echo $category_color_1; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_2)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-2 {
        border-color: <?php echo $category_color_2; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-2 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-2,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-2{
        color: <?php echo $category_color_2; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_3)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-3 {
        border-color: <?php echo $category_color_3; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-3 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-3,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-3{
        color: <?php echo $category_color_3; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_4)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-4 {
        border-color: <?php echo $category_color_4; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-4 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-4,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-4{
        color: <?php echo $category_color_4; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_5)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-5 {
        border-color: <?php echo $category_color_5; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-5 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-5,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-5{
        color: <?php echo $category_color_5; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_6)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-6 {
        border-color: <?php echo $category_color_6; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-6 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-6,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-6{
        color: <?php echo $category_color_6; ?>;
        }
    <?php endif; ?>
    <?php if (!empty($category_color_7)): ?>
        body.aft-default-mode a.newsphere-categories.category-color-7 {
        border-color: <?php echo $category_color_7; ?>;
        }
        body.aft-default-mode .site-footer a.newsphere-categories.category-color-7 ,
        body.aft-default-mode .aft-main-banner-section .color-pad .cat-links li a.newsphere-categories.category-color-7,
        body.aft-default-mode .main-banner-widget-section .color-pad .cat-links li a.newsphere-categories.category-color-7{
        color: <?php echo $category_color_7; ?>;
        }
    <?php endif; ?>

    <?php if (!empty($primary_font)): ?>
        body,
        button,
        input,
        select,
        optgroup,
        textarea, p {
        font-family: <?php echo $primary_font; ?>;
        }
    <?php endif; ?>
    
    <?php if (!empty($secondary_font)): ?>
        .min-read,
        .nav-tabs>li,
        .main-navigation ul li a,
        .site-title, h1, h2, h3, h4, h5, h6 {
        font-family: <?php echo $secondary_font; ?>;
        }
    <?php endif; ?>    

    <?php if (!empty($general_post_title_size)): ?>
        .read-title h4 ,
        .small-gird-style .big-grid .read-title h4,
        .af-double-column.list-style .read-title h4 {
        font-size: <?php echo $general_post_title_size; ?>px;
        }
    <?php endif; ?>

    <?php if (!empty($spotlight_post_title_size)): ?>
        .list-style .read-title h4,
        .aft-trending-latest-popular .small-gird-style .big-grid .read-title h4,
        .newsphere_posts_express_list .grid-part .read-title h4, 
        .af-double-column.list-style .aft-spotlight-posts-1 .read-title h4,
        .banner-carousel-1 .read-title h4 {
        font-size: <?php echo $spotlight_post_title_size; ?>px;
        }
    <?php endif; ?>

    <?php if (!empty($spotlight_post_title_over_image_size)): ?>
        .big-grid .read-title h4 {
        font-size: <?php echo $spotlight_post_title_over_image_size; ?>px;
        }
    <?php endif; ?>

    <?php if (!empty($section_title_font_size)): ?>
        .related-title,
        .widget-title, .header-after1 {
        font-size: <?php echo $section_title_font_size; ?>px;
        }
    <?php endif; ?>

    <?php if (!empty($single_posts_title_size)): ?>
        body.single-post .entry-title {
        font-size: <?php echo $single_posts_title_size; ?>px;
        }
    <?php endif; ?>

    <?php if (!empty($letter_spacing)): ?>
        body,
        .widget-title span, .header-after1 span {
        letter-spacing: <?php echo $letter_spacing; ?>px;
        }
    <?php endif; ?>
    
    <?php if (!empty($line_height)): ?>
        body,
        body.single-post .entry-title,
        .widget-title span, 
        .header-after1 span,
        .read-title h4 {
        line-height: <?php echo $line_height; ?>;
        }
    <?php endif; ?>    
        
       }
        <?php
        return ob_get_clean();
    }
}


