<?php

if ( ! function_exists( 'newsphere_ajax_pagination' ) ) :
	/**
	 * Outputs the required structure for ajax loading posts on scroll and click
	 *
	 * @since 1.0.0
	 * @param $type string Ajax Load Type
	 */
	function newsphere_ajax_pagination($type) {
		if ( $GLOBALS['wp_query']->max_num_pages > 1 ) {
			?>
			<div class="newsphere-load-more-posts" data-load-type="<?php echo esc_attr($type); ?>">
				<a href="#" class="">
                    <span class="ajax-loader">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
					<span class="load-btn"><?php _e('Load More', 'newsphere') ?></span>
				</a>
			</div>
			<?php
		}
	}
endif;

if ( ! function_exists( 'newsphere_load_more' ) ) :
	/**
	 * Ajax Load posts Callback.
	 *
	 * @since 1.0.0
	 *
	 */
	function newsphere_load_more() {

		//echo "herere";die();

		check_ajax_referer( 'newsphere-load-more-nonce', 'nonce' );

		$output['more_post'] = false;
		$output['content'] = array();
        $cat_slug = '';

		$args['post_type'] = ( isset( $_GET['post_type']) && !empty($_GET['post_type'] ) ) ? esc_attr( $_GET['post_type'] ) : 'post';
		$args['post_status'] = 'publish';
		$args['paged'] = (int) esc_attr( $_GET['page'] );

		if( isset( $_GET['cat'] ) && isset( $_GET['taxonomy'] ) ){
			$args['tax_query'] = array(
				array(
					'taxonomy' => esc_attr($_GET['taxonomy']),
					'field'    => 'slug',
					'terms'    => array(esc_attr($_GET['cat'])),
				),
			);
			$cat_slug = $_GET['cat'];
		}

		if( isset($_GET['search']) ){
			$args['s'] = esc_attr( $_GET['search'] );
		}

		if( isset($_GET['author']) ){
			$args['author_name'] = esc_attr( $_GET['author'] );
		}

		if( isset($_GET['year']) || isset($_GET['month']) || isset($_GET['day']) ){

			$date_arr = array();

			if( !empty($_GET['year']) ){
				$date_arr['year'] = (int) esc_attr($_GET['year']);
			}
			if( !empty($_GET['month']) ){
				$date_arr['month'] = (int) esc_attr($_GET['month']);
			}
			if( !empty($_GET['day']) ){
				$date_arr['day'] = (int) esc_attr($_GET['day']);
			}

			if( !empty($date_arr) ){
				$args['date_query'] = array($date_arr);
			}
		}

		$loop = new WP_Query( $args );
		if($loop->max_num_pages > $args['paged']){
			$output['more_post'] = true;
		}
		if ( $loop->have_posts() ):
			$template_part = isset($args['s']) ? 'search' : get_post_type();

			$counter = 1;
			while ( $loop->have_posts() ): $loop->the_post();
				ob_start();

					//get_template_part( 'template-parts/content', $template_part );

                do_action('newsphere_action_archive_layout', $cat_slug);
				$output['content'][] = ob_get_clean();
				$counter++;
			endwhile;wp_reset_postdata();
			wp_send_json_success($output);
		else:
			$output['more_post'] = false;
			wp_send_json_error($output);
		endif;
		wp_die();
	}
endif;
add_action( 'wp_ajax_newsphere_load_more', 'newsphere_load_more' );
add_action( 'wp_ajax_nopriv_newsphere_load_more', 'newsphere_load_more' );