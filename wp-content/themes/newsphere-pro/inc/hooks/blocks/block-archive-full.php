<?php
/**
 * Full block part for displaying page content in page.php
 *
 * @package Newsphere
 */
?>

<div class="read-single">
        <div class="read-details marg-btm-lr"> 

            <?php if ('post' === get_post_type()) : ?>
                <div class="read-categories">
                    <?php newsphere_post_categories(); ?>
                </div>
            <?php endif; ?>
            <div class="read-title">
                <?php the_title('<h4 class="entry-title">
                    <a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a>
                </h4>'); ?>
            </div>            

            <?php if ('post' === get_post_type()) : ?>
                <div class="post-item-metadata entry-meta">
                    <?php newsphere_post_item_meta(); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="read-img pos-rel">
            <?php newsphere_post_thumbnail(); ?>
            <span class="min-read-post-format">
                <?php newsphere_post_format($post->ID); ?>
                <?php newsphere_count_content_words($post->ID); ?>

            </span>
            <a href="<?php the_permalink(); ?>"></a>
            <?php newsphere_get_comments_count($post->ID); ?>
        </div>
        <div class="color-pad">
            <?php
            $archive_content_view = newsphere_get_option('archive_content_view');
            if ($archive_content_view != 'archive-content-none'):
                ?>
            <div class="read-details color-tp-pad no-color-pad"> 


                    <div class="post-description">
                        <?php

                        if ($archive_content_view == 'archive-content-excerpt') {

                            the_excerpt();
                        } else {
                            the_content();
                        }
                        ?>
                    </div>
            </div>
            <?php endif; ?>
        </div>

</div>