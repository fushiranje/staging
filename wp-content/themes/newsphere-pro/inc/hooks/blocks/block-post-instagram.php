<?php
$enable_instagram = newsphere_get_option('footer_show_instagram_post_carousel');
if ($enable_instagram) {
    $username = esc_attr(newsphere_get_option('instagram_username'));
    $number = absint(newsphere_get_option('number_of_instagram_posts'));
    $access_token = esc_attr(newsphere_get_option('instagram_access_token_graphapi'));
    $media_array = newsphere_scrape_instagram($username, $access_token, $number);

    if (is_wp_error($media_array)) {
        echo wp_kses_post($media_array->get_error_message());
    } else { ?>

        <div class="section-block section-insta-block clearfix">
            <div class="insta-slider-wrapper">
                <div class="insta-feed-head">
                    <a href="//instagram.com/<?php echo esc_attr(trim($username)); ?>" rel="me"
                       class="secondary-color secondary-font" target="_blank">
                        <p class="instagram-username"><?php echo '@' . $username; ?></p>
                    </a>
                </div>
                <div class="instagram-carousel af-widget-carousel swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ($media_array as $item) {
                            $media_permalink = $item['original'];

                            if (substr_count($media_permalink, '/') >= 5) {
                                $media_permalink_array = explode('/', $media_permalink);
                                $perm_id = $media_permalink_array[count($media_permalink_array) - 2];
                                $news_media_permalink = 'https://www.instagram.com/p/' . $perm_id . '/';
                                $media_url = $news_media_permalink . 'media?size=m';
                            }
                            ?>

                            <div class="swiper-slide">
                                <a href="<?php echo esc_url($media_permalink) ?>" target="_blank" class="insta-hover">
                                    <figure>
                                        <img src="<?php echo esc_url($item['small']); ?>">
                                    </figure>

                                    <div class="insta-details">
                                        <div class="insta-tb">
                                            <div class="insta-tc">

                                                <p class="insta-logo">
                                                    <i class="fa fa-instagram"></i>
                                                </p>

                                            </div>
                                        </div>

                                    </div>

                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>
        <?php

    }
}
