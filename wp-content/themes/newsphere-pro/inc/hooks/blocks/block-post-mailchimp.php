<?php

$mailchimp_title = esc_html(newsphere_get_option('footer_mailchimp_title'));
$mailchimp_shortcode = wp_kses_post(newsphere_get_option('footer_mailchimp_shortcode'));

if (!empty($mailchimp_shortcode)) {
    ?>
    <div class="mailchimp-block">
        <div class="container-wrapper">
            <h3 class="block-title text-center">
                <?php echo esc_html($mailchimp_title); ?>
            </h3>
        </div>
        <div class="container-wrapper">
            <div class="inner-suscribe">
                <?php echo do_shortcode($mailchimp_shortcode); ?>
            </div>
        </div>
    </div>
    <?php
}