<?php
/**
 * Implement theme metabox.
 *
 * @package Newsphere
 */

if (!function_exists('newsphere_add_theme_meta_box')) :

    /**
     * Add the Meta Box
     *
     * @since 1.0.0
     */
    function newsphere_add_theme_meta_box()
    {

        $screens = array('post', 'page');

        foreach ($screens as $screen) {
            add_meta_box(
                'newsphere-theme-settings',
                esc_html__('Layout Options', 'newsphere'),
                'newsphere_render_layout_options_metabox',
                $screen,
                'side',
                'low'


            );
        }

    }

endif;

add_action('add_meta_boxes', 'newsphere_add_theme_meta_box');

if (!function_exists('newsphere_render_layout_options_metabox')) :

    /**
     * Render theme settings meta box.
     *
     * @since 1.0.0
     */
    function newsphere_render_layout_options_metabox($post, $metabox)
    {

        $post_id = $post->ID;

        // Meta box nonce for verification.
        wp_nonce_field(basename(__FILE__), 'newsphere_meta_box_nonce');
        // Fetch Options list.
        $content_layout = get_post_meta($post_id, 'newsphere-meta-content-alignment', true);
        $content_mode = get_post_meta($post_id, 'newsphere-meta-content-mode', true);

        if (empty($content_layout)) {
            $content_layout = newsphere_get_option('global_content_alignment');
        }


        ?>
        <div id="newsphere-settings-metabox-container" class="newsphere-settings-metabox-container">
            <div id="newsphere-settings-metabox-tab-layout">
                <div class="newsphere-row-content">
                    <!-- Select Field-->
                    <p>

                        <select name="newsphere-meta-content-alignment" id="newsphere-meta-content-alignment">

                            <option value="" <?php selected('', $content_layout); ?>>
                                <?php _e('Set as global layout', 'newsphere') ?>
                            </option>
                            <option value="align-content-left" <?php selected('align-content-left', $content_layout); ?>>
                                <?php _e('Content - Primary Sidebar', 'newsphere') ?>
                            </option>
                            <option value="align-content-right" <?php selected('align-content-right', $content_layout); ?>>
                                <?php _e('Primary Sidebar - Content', 'newsphere') ?>
                            </option>
                            <option value="full-width-content" <?php selected('full-width-content', $content_layout); ?>>
                                <?php _e('Full width content', 'newsphere') ?>
                            </option>
                        </select>
                    </p>

                    <p>
                    <h4><?php _e('Content Mode', 'newsphere') ?></h4>

                    <select name="newsphere-meta-content-mode" id="newsphere-meta-content-mode">

                        <option value="" <?php selected('', $content_mode); ?>>
                            <?php _e('Set as global layout', 'newsphere') ?>
                        </option>
                        <option value="single-content-mode-default" <?php selected('single-content-mode-default', $content_mode); ?>>
                            <?php _e('Wide - Default', 'newsphere') ?>
                        </option>
                        <option value="single-content-mode-boxed" <?php selected('single-content-mode-boxed', $content_mode); ?>>
                            <?php _e('Boxed', 'newsphere') ?>
                        </option>

                    </select>
                    </p>

                </div><!-- .newsphere-row-content -->
            </div><!-- #newsphere-settings-metabox-tab-layout -->
        </div><!-- #newsphere-settings-metabox-container -->

        <?php
    }

endif;


if (!function_exists('newsphere_save_layout_options_meta')) :

    /**
     * Save theme settings meta box value.
     *
     * @since 1.0.0
     *
     * @param int $post_id Post ID.
     * @param WP_Post $post Post object.
     */
    function newsphere_save_layout_options_meta($post_id, $post)
    {

        // Verify nonce.
        if (!isset($_POST['newsphere_meta_box_nonce']) || !wp_verify_nonce($_POST['newsphere_meta_box_nonce'], basename(__FILE__))) {
            return;
        }

        // Bail if auto save or revision.
        if (defined('DOING_AUTOSAVE') || is_int(wp_is_post_revision($post)) || is_int(wp_is_post_autosave($post))) {
            return;
        }

        // Check the post being saved == the $post_id to prevent triggering this call for other save_post events.
        if (empty($_POST['post_ID']) || $_POST['post_ID'] != $post_id) {
            return;
        }

        // Check permission.
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return;
            }
        } else if (!current_user_can('edit_post', $post_id)) {
            return;
        }

        $content_mode = isset($_POST['newsphere-meta-content-mode']) ? $_POST['newsphere-meta-content-mode'] : '';
        update_post_meta($post_id, 'newsphere-meta-content-mode', sanitize_text_field($content_mode));

        $content_layout = isset($_POST['newsphere-meta-content-alignment']) ? $_POST['newsphere-meta-content-alignment'] : '';
        update_post_meta($post_id, 'newsphere-meta-content-alignment', sanitize_text_field($content_layout));


    }

endif;

add_action('save_post', 'newsphere_save_layout_options_meta', 10, 2);


//Category fields meta starts


if (!function_exists('newsphere_taxonomy_add_new_meta_field')) :
// Add term page
    function newsphere_taxonomy_add_new_meta_field()
    {
        // this will add the custom meta field to the add new term page

        $cat_color = array(
            'category-color-1' => __('Category Color 1', 'newsphere'),
            'category-color-2' => __('Category Color 2', 'newsphere'),
            'category-color-3' => __('Category Color 3', 'newsphere'),
            'category-color-4' => __('Category Color 4', 'newsphere'),
            'category-color-5' => __('Category Color 5', 'newsphere'),
            'category-color-6' => __('Category Color 6', 'newsphere'),
            'category-color-7' => __('Category Color 7', 'newsphere'),


        );
        ?>
        <div class="form-field">
            <label for="term_meta[color_class_term_meta]"><?php _e('Color Class', 'newsphere'); ?></label>
            <select id="term_meta[color_class_term_meta]" name="term_meta[color_class_term_meta]">
                <?php foreach ($cat_color as $key => $value): ?>
                    <option value="<?php echo esc_attr($key); ?>"><?php echo esc_html($value); ?></option>
                <?php endforeach; ?>
            </select>
            <p class="description"><?php _e('Select category color class. You can set appropriate categories color on "Categories" section of the theme customizer.', 'newsphere'); ?></p>
        </div>
        <?php
    }
endif;
add_action('category_add_form_fields', 'newsphere_taxonomy_add_new_meta_field', 10, 2);


if (!function_exists('newsphere_taxonomy_edit_meta_field')) :
// Edit term page
    function newsphere_taxonomy_edit_meta_field($term)
    {

        // put the term ID into a variable
        $t_id = $term->term_id;

        // retrieve the existing value(s) for this meta field. This returns an array
        $term_meta = get_option("category_color_$t_id");

        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label
                        for="term_meta[color_class_term_meta]"><?php _e('Color Class', 'newsphere'); ?></label></th>
            <td>
                <?php
                $cat_color = array(
                    'category-color-1' => __('Category Color 1', 'newsphere'),
                    'category-color-2' => __('Category Color 2', 'newsphere'),
                    'category-color-3' => __('Category Color 3', 'newsphere'),
                    'category-color-4' => __('Category Color 4', 'newsphere'),
                    'category-color-5' => __('Category Color 5', 'newsphere'),
                    'category-color-6' => __('Category Color 6', 'newsphere'),
                    'category-color-7' => __('Category Color 7', 'newsphere'),


                );
                ?>
                <select id="term_meta[color_class_term_meta]" name="term_meta[color_class_term_meta]">
                    <?php foreach ($cat_color as $key => $value): ?>
                        <option value="<?php echo esc_attr($key); ?>"<?php selected($term_meta['color_class_term_meta'], $key); ?> ><?php echo esc_html($value); ?></option>
                    <?php endforeach; ?>
                </select>
                <p class="description"><?php _e('Select category color class. You can set appropriate categories color on "Categories" section of the theme customizer.', 'newsphere'); ?></p>
            </td>
        </tr>
        <?php
    }
endif;
add_action('category_edit_form_fields', 'newsphere_taxonomy_edit_meta_field', 10, 2);




if (!function_exists('save_taxonomy_color_class_meta')) :
// Save extra taxonomy fields callback function.
    function save_taxonomy_color_class_meta($term_id)
    {
        if (isset($_POST['term_meta'])) {
            $t_id = $term_id;
            $term_meta = get_option("category_color_$t_id");
            $cat_keys = array_keys($_POST['term_meta']);
            foreach ($cat_keys as $key) {
                if (isset ($_POST['term_meta'][$key])) {
                    $term_meta[$key] = $_POST['term_meta'][$key];
                }
            }
            // Save the option array.
            update_option("category_color_$t_id", $term_meta);
        }
    }

endif;
add_action('edited_category', 'save_taxonomy_color_class_meta', 10, 2);
add_action('create_category', 'save_taxonomy_color_class_meta', 10, 2);

//category archive layout

if (!function_exists('newsphere_taxonomy_add_new_layout_meta_field')) :
// Add term page
	function newsphere_taxonomy_add_new_layout_meta_field()
	{
		// this will add the custom meta field to the add new term page

		$cat_archive_layout = array(
			'archive-layout-list' => __('list', 'newsphere'),
			'archive-layout-grid' => __('Grid', 'newsphere'),
			'archive-layout-full' => __('Full', 'newsphere'),
			'archive-layout-masonry' => __('Masonary', 'newsphere')

		);

		$archive_list_alignment = array(
		  'archive-image-left'=>__('Left','newsphere'),
		  'archive-image-right'=>__('Right','newsphere'),
		  'archive-image-alternate'=>__('Alternate','newsphere'),
        );

		$archive_grid_alignment = array(
			'archive-image-default'=>__('Default','newsphere'),
			'archive-image-up-alternate'=>__('Alternate','newsphere'),
			'archive-image-full-alternate'=>__('Alternate with full','newsphere'),
			'archive-image-list-alternate'=>__('Alternate with list','newsphere'),
		);

		$archive_mode = newsphere_get_option('archive_layout');
		$archive_mode_allignment_list = newsphere_get_option('archive_image_alignment');
		$archive_mode_allignment_grid = newsphere_get_option('archive_image_alignment_grid');

		?>
        <div class="form-field">
            <label for="term_meta[archive_layout_term_meta]"><?php _e('Archive layout', 'newsphere'); ?></label>
            <select id="term_meta[archive_layout_term_meta]" class="archive_opt" name="term_archive_layout[archive_layout_term_meta]">
				<?php foreach ($cat_archive_layout as $key => $value): ?>
                    <option value="<?php echo esc_attr($key); ?>" <?php selected($archive_mode, $key); ?>><?php echo esc_html($value); ?></option>
				<?php endforeach; ?>
            </select>
            <p class="description"><?php _e('Select layout for archive', 'newsphere'); ?></p>
        </div>

        <div class="archive-list-alignment" style="display: none">
        <div class="form-field">
            <label for="term_meta_list[archive_layout_alignment_term_meta_list]"><?php _e('Archive Alignment', 'newsphere'); ?></label>
            <select id="term_meta_list[archive_layout_alignment_term_meta_list]" class="archive_opt_list" name="term_archive_layout_list[archive_layout_alignment_term_meta_list]">
				<?php foreach ($archive_list_alignment as $key => $value): ?>
                    <option value="<?php echo esc_attr($key); ?>" <?php selected($archive_mode_allignment_list , $key); ?>><?php echo esc_html($value); ?></option>
				<?php endforeach; ?>
            </select>
            <p class="description"><?php _e('Select alignment for list archive layout', 'newsphere'); ?></p>
        </div>
        </div>

        <div class="archive-grid-alignment" style="display: none">
            <div class="form-field">
                <label for="term_meta_grid[archive_layout_alignment_term_meta_grid]"><?php _e('Archive Alignment', 'newsphere'); ?></label>
                <select id="term_meta_gird[archive_layout_alignment_term_meta_grid]" class="archive_opt_gird" name="term_archive_layout_align_gird[archive_layout_alignment_term_meta_gird]">
					<?php foreach ($archive_grid_alignment as $key => $value): ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php selected($archive_mode_allignment_grid, $key); ?>><?php echo esc_html($value); ?></option>
					<?php endforeach; ?>
                </select>
                <p class="description"><?php _e('Select alignment for gird archive layout', 'newsphere'); ?></p>
            </div>
        </div>

		<?php
	}
endif;
add_action('category_add_form_fields', 'newsphere_taxonomy_add_new_layout_meta_field', 10, 2);

//Edit archive layout

if (!function_exists('newsphere_taxonomy_edit_layout_meta_field')) :
// Edit term page
	function newsphere_taxonomy_edit_layout_meta_field($term)
	{

		// put the term ID into a variable
		$t_id = $term->term_id;

		// retrieve the existing value(s) for this meta field. This returns an array
		$term_meta = get_option("category_layout_$t_id");
		$term_meta_list = get_option("category_layout_list_$t_id");
		$term_meta_grid = get_option("category_layout_grid_$t_id");

		if(!empty($term_meta)){
		    $term_meta = $term_meta['archive_layout_term_meta'];
        }else{
		    $term_meta = newsphere_get_option('archive_layout');
        }

		if(!empty($term_meta_list)){
		    $archive_layout_list = $term_meta_list['archive_layout_alignment_term_meta_list'];

        }else{

			$archive_layout_list = newsphere_get_option('archive_image_alignment');

        }

		if(!empty($term_meta_grid)){
			$archive_layout_grid = $term_meta_grid['archive_layout_alignment_term_meta_gird'];
        }else{
			$archive_layout_grid = newsphere_get_option('archive_image_alignment_grid');;
        }

		?>
        <tr class="form-field">
            <th scope="row" valign="top"><label
                        for="term_meta[archive_layout_term_meta]"><?php _e('Archive layout', 'newsphere'); ?></label></th>
            <td>
				<?php
				$cat_archive_layout = array(
					'archive-layout-list' => __('list', 'newsphere'),
					'archive-layout-grid' => __('Grid', 'newsphere'),
					'archive-layout-full' => __('Full', 'newsphere'),
					'archive-layout-masonry' => __('Masonary', 'newsphere')

				);

				$archive_list_alignment = array(
					'archive-image-left'=>__('Left','newsphere'),
					'archive-image-right'=>__('Right','newsphere'),
					'archive-image-alternate'=>__('Alternate','newsphere'),
				);

				$archive_grid_alignment = array(
					'archive-image-default'=>__('Default','newsphere'),
					'archive-image-up-alternate'=>__('Alternate','newsphere'),
					'archive-image-full-alternate'=>__('Alternate with full','newsphere'),
					'archive-image-list-alternate'=>__('Alternate with list','newsphere'),
				);

				?>
                <select id="term_meta[archive_layout_term_meta]" class="archive_opt" name="term_archive_layout[archive_layout_term_meta]">
					<?php foreach ($cat_archive_layout as $key => $value): ?>
                        <option value="<?php echo esc_attr($key); ?>"<?php selected($term_meta, $key); ?> ><?php echo esc_html($value); ?></option>
					<?php endforeach; ?>
                </select>
                <p class="description"><?php _e('Select layout for archive', 'newsphere'); ?></p>
            </td>
        </tr>

        <tr class="form-field archive-list-alignment" style="display: none">
            <th scope="row" valign="top"><label
                        for="term_meta_list[archive_layout_alignment_term_meta_list]"><?php _e('Archive Alignment', 'newsphere'); ?></label></th>
            <td>
                <select id="term_meta_list[archive_layout_alignment_term_meta_list]" class="archive_opt_list" name="term_archive_layout_list[archive_layout_alignment_term_meta_list]">
		            <?php foreach ($archive_list_alignment as $key => $value): ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php selected($archive_layout_list , $key); ?>><?php echo esc_html($value); ?></option>
		            <?php endforeach; ?>
                </select>
                <p class="description"><?php _e('Select alignment for list archive layout', 'newsphere'); ?></p>
            </td>
        </tr>


        <tr class="form-field archive-grid-alignment" style="display: none">
            <th scope="row" valign="top"><label
                        for="term_meta_grid[archive_layout_alignment_term_meta_grid]"><?php _e('Archive Alignment', 'newsphere'); ?></label></th>
            <td>
                <select id="term_meta_gird[archive_layout_alignment_term_meta_grid]" class="archive_opt_gird" name="term_archive_layout_align_gird[archive_layout_alignment_term_meta_gird]">
		            <?php foreach ($archive_grid_alignment as $key => $value): ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php selected($archive_layout_grid, $key); ?>><?php echo esc_html($value); ?></option>
		            <?php endforeach; ?>
                </select>
                <p class="description"><?php _e('Select alignment for gird archive layout', 'newsphere'); ?></p>
            </td>
        </tr>


		<?php
	}
endif;
add_action('category_edit_form_fields', 'newsphere_taxonomy_edit_layout_meta_field', 10, 2);

//category archive layout save

if (!function_exists('save_taxonomy_archive_layout_meta')) :
// Save extra taxonomy fields callback function.
	function save_taxonomy_archive_layout_meta($term_id)
	{
		if (isset($_POST['term_archive_layout'])) {

		    $t_id = $term_id;
			$term_meta = get_option("category_layout_$t_id");
			$cat_keys = array_keys($_POST['term_archive_layout']);
			foreach ($cat_keys as $key) {
				if (isset ($_POST['term_archive_layout'][$key])) {
					$term_meta[$key] = $_POST['term_archive_layout'][$key];
				}
			}
			// Save the option array.
			update_option("category_layout_$t_id", $term_meta);
		}

		if(isset($_POST['term_archive_layout_list'])){
			$t_id = $term_id;
			$term_meta = get_option("category_layout_list_$t_id");
			$list_keys = array_keys($_POST['term_archive_layout_list']);
			foreach ($list_keys as $key) {
				if (isset ($_POST['term_archive_layout_list'][$key])) {
					$term_meta[$key] = $_POST['term_archive_layout_list'][$key];
				}
			}
			// Save the option array.
			update_option("category_layout_list_$t_id", $term_meta);
		}

		if(isset($_POST['term_archive_layout_align_gird'])){
			$t_id = $term_id;
			$term_meta = get_option("category_layout_grid_$t_id");
			$gird_keys = array_keys($_POST['term_archive_layout_align_gird']);
			foreach ($gird_keys as $key) {
				if (isset ($_POST['term_archive_layout_align_gird'][$key])) {
					$term_meta[$key] = $_POST['term_archive_layout_align_gird'][$key];
				}
			}
			// Save the option array.
			update_option("category_layout_grid_$t_id", $term_meta);
		}
	}

endif;
add_action('edited_category', 'save_taxonomy_archive_layout_meta', 10, 2);
add_action('create_category', 'save_taxonomy_archive_layout_meta', 10, 2);


//Category fields meta ends