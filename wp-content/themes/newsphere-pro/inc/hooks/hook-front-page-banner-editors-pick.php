<?php
if (!function_exists('newsphere_banner_editors_pick')):
    /**
     * Ticker Slider
     *
     * @since Newsphere 1.0.0
     *
     */
    function newsphere_banner_editors_pick()
    {
        $color_class = 'category-color-1';
        ?>
        <?php
        $show_editors_pick_section = newsphere_get_option('show_editors_pick_section');
        if ($show_editors_pick_section):
            $editors_pick_section_title = newsphere_get_option('editors_pick_section_title');
            $dir = 'ltr';
            if (is_rtl()) {
                $dir = 'rtl';
            }
            ?>
            <div class="af-main-banner-featured-posts featured-posts" dir="<?php echo esc_attr($dir); ?>">
                <?php if (!empty($editors_pick_section_title)): ?>
                    <h4 class="header-after1 ">
                                <span class="header-after <?php echo esc_attr($color_class); ?>">
                                    <?php echo esc_html($editors_pick_section_title); ?>
                                </span>
                    </h4>
                <?php endif; ?>
                <div class="section-wrapper">
                    <div class="small-gird-style af-container-row clearfix">
                        <?php
                        $newsphere_editors_pick_category = newsphere_get_option('select_editors_pick_category');
                        $newsphere_number_of_editors_pick_news = newsphere_get_option('number_of_editors_pick_news');

                        $featured_posts = newsphere_get_posts($newsphere_number_of_editors_pick_news, $newsphere_editors_pick_category);
                        if ($featured_posts->have_posts()) :
                            while ($featured_posts->have_posts()) :
                                $featured_posts->the_post();
                                $aft_post_id = get_the_ID();
                                $url = newsphere_get_freatured_image_url($aft_post_id, 'newsphere-medium');
                                ?>
                                <div class="col-4 pad float-l big-grid af-sec-post">
                                    <div class="read-single pos-rel">
                                        <div class="data-bg read-img pos-rel read-bg-img"
                                             data-background="<?php echo esc_url($url); ?>">
                                            <img src="<?php echo esc_url($url); ?>">
                                            <a href="<?php the_permalink(); ?>"></a>
                                            <?php newsphere_get_comments_count($aft_post_id); ?>
                                        </div>
                                        <div class="read-details">
	  							<span class="min-read-post-format">
		  								<?php newsphere_post_format($aft_post_id); ?>
                                        <?php newsphere_count_content_words($aft_post_id); ?>

                                        </span>
                                            <div class="read-categories">
                                                <?php newsphere_post_categories(); ?>
                                            </div>
                                            <div class="read-title">
                                                <h4>
                                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                </h4>
                                            </div>
                                            <div class="entry-meta">
                                                <?php newsphere_post_item_meta(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>

        <!-- Editors Pick line END -->
        <?php

    }
endif;

add_action('newsphere_action_banner_editors_pick', 'newsphere_banner_editors_pick', 10);