=== Newsphere Pro===

Contributors: AF themes
Tested up to: 5.6
Stable tag: 2.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: news, blog, entertainment, one-column, two-columns, left-sidebar, right-sidebar, custom-background, custom-menu, featured-images, full-width-template, custom-header, translation-ready, theme-options, threaded-comments

A starter theme called Newsphere.

== Copyright ==

This theme, like WordPress, is licensed under the GPL.
Use it to make something cool, have fun, and share what you've learned with others.

Newsphere Pro is based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL v2 or later.

Newsphere WordPress Theme, Copyright 2018 AF themes
Newsphere is distributed under the terms of the GNU GPL v2 or later.

== Description ==
Newsphere Pro is a perfect news and magazine responsive WordPress theme that lets you write articles and blog posts with ease. Create a great news website the help of live customizer options and custom widgets, you can design your website as you like and preview the changes live. As it includes many useful features that require to build an awesome looking newsportal, the theme is perfect for blogging and excellent for a news, newspaper, magazine, publishing or review site. It is also compatible with the Gutenberg, RTL, MailChimp, Instagram feeds along with WooCommerce plugin which helps you to integrate an online business with our newspaper template. the theme is well optimized that helps to rank your website in the top of search engines and users will get an outstanding support from the team if there will be any difficulties while using the theme. There are number of demos available in this theme so choose the one you like and start to build a website. See our demos: https://demo.afthemes.com/newsphere-pro/, https://demo.afthemes.com/newsphere-pro/dark, https://demo.afthemes.com/newsphere-pro/light, https://demo.afthemes.com/newsphere-pro/sport-pro/, https://demo.afthemes.com/newsphere-pro/fashion-pro/,  https://demo.afthemes.com/newsphere-pro/travel-pro/, https://demo.afthemes.com/newsphere-pro/recipe-pro/, https://demo.afthemes.com/storymag-pro/minimal-pro/, https://demo.afthemes.com/storymag-pro/masonry-pro/

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Newsphere Pro includes support for Infinite Scroll in Jetpack.

== Change log ==
= 2.0.0 - Jan 07 2021 =
* Improved - jQuery migrate
* Improved - Sticky sidebar
* Improved - Texts readability
* Fixed - Tabs active issue
* Fixed some design glitch

= 1.0.7 - Sept 23 2020 =
* New Instagram access token field based on Graph API
* Single post content mode option added
* Improved mobile accessibility
* Fixed some design glitch

= 1.0.6 - Sept 14 2020 =
* Top header date format as set in WP date setting

= 1.0.5 - Aug 26 2020 =
* Improved content page
* Improved content design
* Fixed some design glitch
* Improved breadcrumbs
* Tested latest WordPress compatibility

= 1.0.6 - Aug 26 2020 =

= 1.0.4 - July 11 2019 =
* Updates - Breadcrumbs schema.
* Updates - Top Header date format to default WP Settings.
* Fixes - IE/Edge dropdown glitch.
* Fixes - Some styling glitch.

= 1.0.3 - June 27 2019 =
* New - Tabbed title section for "Latest" and "Popular" have been added
* Fixed some design glitch

= 1.0.2 - May 24 2019 =
* Fixed license URL glitch

= 1.0.1 - May 24 2019 =
* Fixed some design glitch

= 1.0.0 - May 02 2019 =
* initial release


== Credits ==

Underscores:
Author: 2012-2015 Automattic
Source: http://underscores.me
License: GPL v2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

normalize:
Author: 2012-2015 Nicolas Gallagher and Jonathan Neal
Source: http://necolas.github.io/normalize.css
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

html5shiv:
Author: 2014 Alexander Farkas (aFarkas)
Source: https://github.com/afarkas/html5shiv
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

Bootstrap:
Author: Twitter
URL: http://getbootstrap.com
License: Licensed under the MIT license

Font Awesome:
Author: Dave Gandy
URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL
License: Licensed under SIL Open Font License (OFL) and MIT License

slick carousel:
Author: Ken Wheeler
URL: https://github.com/kenwheeler/slick
License: Licensed under the MIT license

jquery-match-height:
Author: liabru
URL: http://brm.io/jquery-match-height/
License: MIT

jQuery.marquee:
Author: Aamir Afridi
URL:  http://aamirafridi.com/jquery/jquery-marquee-plugin
License: MIT

Breadcrumb Trail:
Author: Justin Tadlock
URL:  https://github.com/justintadlock/breadcrumb-trail/blob/master/breadcrumb-trail.php
License: GPLv2 or later

TGM-Plugin-Activation:
Author: Thomas Griffin (thomasgriffinmedia.com)
URL:  https://github.com/TGMPA/TGM-Plugin-Activation
License: GNU General Public License, version 2


------------------------------------------------------------------------------
Google Fonts
------------------------------------------------------------------------------

Font Name: Lato
Font URL: https://fonts.google.com/specimen/Lato
Font License: Open Font License

Font Name: Source Sans Pro
Font URL: https://fonts.google.com/specimen/Source+Sans+Pro
Font License: Open Font License


------------------------------------------------------------------------------
Image License
------------------------------------------------------------------------------
https://stocksnap.io/photo/SFKZHJODOV
https://stocksnap.io/photo/U381E1SOZI
https://stocksnap.io/photo/NWAPXFUMDW
https://stocksnap.io/photo/EISDMHRMFR
https://stocksnap.io/photo/W2QMUQHOGE
https://stocksnap.io/photo/LPDQBLM2A0