/*jshint browser:true */
/*!
* FitVids 1.1
* Copyright 2013, Chris Coyier - https://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
*/
$ = jQuery;
!(function(t) {
    "use strict";
    (t.fn.fitVids = function(e) {
        var i = { customSelector: null, ignore: null };
        if (!document.getElementById("fit-vids-style")) {
            var r = document.head || document.getElementsByTagName("head")[0],
                a =
                    ".fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}",
                d = document.createElement("div");

            (d.innerHTML = '<p>x</p><style id="fit-vids-style">' + a + "</style>"),
                r.appendChild(d.childNodes[1]);
        }
        return (
            e && t.extend(i, e),
                this.each(function() {
                    var e = [
                        'iframe[src*="player.vimeo.com"]',
                        'iframe[src*="youtube.com"]',
                        'iframe[src*="youtube-nocookie.com"]',
                        'iframe[src*="kickstarter.com"][src*="video.html"]',
                        "object",
                        "embed"
                    ];
                    i.customSelector && e.push(i.customSelector);
                    var r = ".fitvidsignore";
                    i.ignore && (r = r + ", " + i.ignore);
                    var a = t(this).find(e.join(","));
                    (a = a.not("object object")),
                        (a = a.not(r)),
                        a.each(function() {
                            var e = t(this);
                            if (
                                !(
                                    e.parents(r).length > 0 ||
                                    ("embed" === this.tagName.toLowerCase() &&
                                        e.parent("object").length) ||
                                    e.parent(".fluid-width-video-wrapper").length
                                )
                            ) {
                                e.css("height") ||
                                e.css("width") ||
                                (!isNaN(e.attr("height")) && !isNaN(e.attr("width"))) ||
                                (e.attr("height", 9), e.attr("width", 16));
                                var i =
                                        "object" === this.tagName.toLowerCase() ||
                                        (e.attr("height") && !isNaN(parseInt(e.attr("height"), 10)))
                                            ? parseInt(e.attr("height"), 10)
                                            : e.height(),
                                    a = isNaN(parseInt(e.attr("width"), 10))
                                        ? e.width()
                                        : parseInt(e.attr("width"), 10),
                                    d = i / a;
                                if (!e.attr("name")) {
                                    var o = "fitvid" + t.fn.fitVids._count;
                                    e.attr("name", o), t.fn.fitVids._count++;
                                }
                                e
                                    .wrap('<div class="fluid-width-video-wrapper"></div>')
                                    .parent(".fluid-width-video-wrapper")
                                    .css("padding-top", 100 * d + "%"),
                                    e.removeAttr("height").removeAttr("width");
                            }
                        });
                })
        );
    }),
        (t.fn.fitVids._count = 0);
})(window.jQuery || window.Zepto);


/* This is what makes the videos responsive. You can either include this in a <script> tag in the markup, or better yet, include it in a separata JavaScript file.*/
$(function() {

    //hide iframe and display youtube banner image on click
    $('.af-custom-thumbnail').on('click',function(){
        $('.af-hide-iframe').addClass('af-display-iframe');
        if($('.fluid-width-video-wrapper').length > 0){
            $('.fluid-width-video-wrapper').addClass('af-iframe-added');
        }

        $('#vid_frame').removeAttr('src');
        $('.af-video-wrap').removeAttr('data-video-link');
        var selected_vdo_link = $(this).data('video');
        $('.af-video-wrap').attr('data-video-link', selected_vdo_link);
        var img = $(this).data('item');
        $('.af-video-wrap').css('background-image', 'url(' + img + ')');
        $('.af-video-wrap').css('padding-top', '50%');
        //


    });

    $('.af-video-wrap').on('click',function(){
        $('#vid_frame').removeAttr('src');
        $('.af-hide-iframe').removeClass('af-display-iframe');
        $('.fluid-width-video-wrapper').removeClass('af-iframe-added');

        $(this).css('padding-top',0);
        var video_link = $(this).attr('data-video-link');

        $('#vid_frame').attr('src', video_link);
        $(".vid-container").fitVids();
    });

});

/*JS FOR SCROLLING THE ROW OF THUMBNAILS*/

$(document).ready(function() {
    var active_banner = $('.first_thb_img').data('item');
    var first_video = $('.first_thb_img').data('video');
    $('.af-video-wrap').css('padding-top', '50%');
    $('.af-hide-iframe').addClass('af-display-iframe');

    $('.af-video-wrap').css('background-image', 'url(' + active_banner + ')');
    $('.af-video-wrap').attr('data-video-link', first_video);

    $(".vid-item").each(function(index) {
        $(this).on("click", function() {
            var current_index = index + 1;
            $(".vid-item .thumb").removeClass("active");
            $(".vid-item:nth-child(" + current_index + ") .thumb").addClass("active");
        });
    });
});