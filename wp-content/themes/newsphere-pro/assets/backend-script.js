jQuery(document).ready(function($){

    var archive_selected_name = $('.archive_opt').val();
    if(archive_selected_name == 'archive-layout-list'){
        $('.archive-list-alignment').toggle();
        $('.archive-grid-alignment').hide();
    }
    if(archive_selected_name == 'archive-layout-grid') {
        $('.archive-grid-alignment').toggle();
        $('.archive-list-alignment').hide();

    }


    $('.archive_opt').on('change',function () {
        var archive_layout_name = $(this).val();
        if(archive_layout_name == 'archive-layout-list'){
            $('.archive-list-alignment').toggle();
            $('.archive-grid-alignment').hide();

        }
        else if(archive_layout_name == 'archive-layout-grid'){
            $('.archive-list-alignment').hide();
            $('.archive-grid-alignment').toggle();
        }else{
            $('.archive-list-alignment').hide();
            $('.archive-grid-alignment').hide();
        }
    });

});