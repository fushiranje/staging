<?php
// Creating the widget
class ban_widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'ban_widget',
            __('Banner Ad', 'ban_widget_domain'),
            array(
            'description' => __('Custom ad banner')
        ));
    }
    // Creating widget front-end
    // This is where the action happens
    public function widget($args, $instance)
    {
		// die();
        $hide_on_mobile = apply_filters('widget_title', $instance['hide_on_mobile']);
        $image = apply_filters('widget_title', $instance['image']);
        $url = apply_filters('widget_title', $instance['url']);
        $width = apply_filters('widget_title', $instance['width']);
        $height = apply_filters('widget_title', $instance['height']);
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
		echo '<a href="'.$url.'" target="_blank" class="mobile-ad-banner-custom"><img src="'.$image.'" style="width:100%;"></a>';
		// This is where you run the code and display the output
		if($hide_on_mobile == "true")
		{
			?>
			<style>
				@media only screen and (max-width: 500px) {
				.mobile-ad-banner-custom  {
					display: none !important;}
				}
				</style>
			<?php
		} // if

        echo $args['after_widget'];
    }
    // Widget Backend
    public function form($instance)
    {
		// Redirect url
		if (isset($instance['hide_on_mobile'])) {
            $hide_on_mobile = $instance['hide_on_mobile'];
        } else {
            $hide_on_mobile = '';
        }

		// image url 
        if (isset($instance['image'])) {
            $image = $instance['image'];
        } else {
            $image = '';
		}
		// Redirect url
		if (isset($instance['url'])) {
            $url = $instance['url'];
        } else {
            $url = '';
		}
		// width
		if (isset($instance['width'])) {
			$width = $instance['width'];
		} else {
			$width = '';
		}

		// height
		if (isset($instance['height'])) {
            $height = $instance['height'];
        } else {
            $height = '';
		}

		



		?>

<!-- image url -->
<br>
<br>
<label for="<?php
        echo $this->get_field_id('hide_on_mobile');
		?>"><?php
        _e('Hide on mobile:');
		?>
<input class="widefat" id="<?php
        echo $this->get_field_id('hide_on_mobile');
		?>" name="<?php
        echo $this->get_field_name('hide_on_mobile');
		?>" type="checkbox" value="<?php
        echo esc_attr($hide_on_mobile);
?>" />

<br>
<br>
<!-- image url -->
<label for="<?php
        echo $this->get_field_id('image');
?>"><?php
        _e('Image:');
?>
<input class="widefat" id="<?php
        echo $this->get_field_id('image');
?>" name="<?php
        echo $this->get_field_name('image');
?>" type="text" value="<?php
        echo esc_attr($image);
?>" />
<br>
<br>
<!-- redirect url -->
<label for="<?php
        echo $this->get_field_id('url');
?>"><?php
        _e('url:');
?>
<input class="widefat" id="<?php
        echo $this->get_field_id('url');
?>" name="<?php
        echo $this->get_field_name('url');
?>" type="text" value="<?php
        echo esc_attr($url);
?>" />
<br>
<br>

<!-- width -->
<label for="<?php
        echo $this->get_field_id('width');
?>"><?php
        _e('width:');
?>
<input class="widefat" id="<?php
        echo $this->get_field_id('width');
?>" name="<?php
        echo $this->get_field_name('width');
?>" type="text" value="<?php
        echo esc_attr($width);
?>" />
<br>
<br>
<!-- height -->
<label for="<?php
        echo $this->get_field_id('height');
?>"><?php
        _e('height:');
?>
<input class="widefat" id="<?php
        echo $this->get_field_id('height');
?>" name="<?php
        echo $this->get_field_name('height');
?>" type="text" value="<?php
        echo esc_attr($height);
?>" />
<br>
<br>


<?php
    }
    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
	{
			$instance          = array();
			$instance['hide_on_mobile'] = (!empty($new_instance['hide_on_mobile']))? strip_tags($new_instance['hide_on_mobile']) : '';
			$instance['image'] = (!empty($new_instance['image']))? strip_tags($new_instance['image']) : '';
			$instance['url'] = (!empty($new_instance['url']))? strip_tags($new_instance['url']) : '';
			$instance['width'] = (!empty($new_instance['width']))? strip_tags($new_instance['width']) : '';
			$instance['height'] = (!empty($new_instance['height']))? strip_tags($new_instance['height']) : '';
			return $instance;
		}
	} // Class ban_widget ends here

	// Register and load the widget
	function ban_load_widget()
	{
		register_widget('ban_widget');
	}
	add_action('widgets_init', 'ban_load_widget');