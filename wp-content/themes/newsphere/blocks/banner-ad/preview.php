<?php
	if(!empty(block_field( 'ad-title',false ))){
	?>
<h3 class="has-text-align-center"><?php block_field( 'ad-title') ?></h3>
<?php
}
?>
<img src="<?php block_field( 'image' ); ?>" alt="<?php block_field( 'alt-text' ); ?>">