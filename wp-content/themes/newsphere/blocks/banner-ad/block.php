<?php
// echo "aaaaaaaaaaaaaa";
// var_dump(block_field('hide-on-desktop',$echo = false));
// echo "aaaaaaaaaaaaaa";
$hide_on_desktop = block_field( 'hide-on-desktop',false);
$hide_on_mobile = block_field( 'hide-on-mobile',false);
?>



<?php
	if(!empty(block_field( 'ad-title',false ))){
	?>
<hr class="wp-block-separator is-style-wide <?php if( $hide_on_desktop == true ) { ?> hide-desktop <?php } ?> <?php if( $hide_on_mobile == true ) { ?> hide-mobile <?php } ?> ">
<h3 class="has-text-align-center <?php if( $hide_on_desktop == true ) { ?> hide-desktop <?php } ?> <?php if( $hide_on_mobile == true ) { ?> hide-mobile <?php } ?> "><a rel="noreferrer noopener" href="<?php block_field( 'redirect-url' ); ?>" target="_blank"><?php block_field( 'ad-title') ?></a></h3>
<?php
}
?>

<a href="<?php block_field( 'redirect-url' ); ?>" target="_blank" class="<?php if( $hide_on_desktop == true ) { ?> hide-desktop <?php } ?> <?php if( $hide_on_mobile == true ) { ?> hide-mobile <?php } ?> " > 
	<img src="<?php block_field( 'image' ); ?>" alt="<?php block_field( 'alt-text' ); ?>" 
	
	 style="<?php
	 if(!empty(block_field( 'width',false ))){
		echo "width:";
		block_field( 'width' );
		echo "px;";
	 }
	 if(!empty(block_field( 'height',false ))){
		echo "height:";
		block_field( 'height' );
		echo "px;";
	 }
	 ?>" 
	 >
</a>

<style>

@media only screen and (min-width: 629px) {
	.hide-desktop  {
		display: none !important;
	}
}

@media only screen and (max-width:629px) {
	.hide-mobile  {
		display: none !important;
	}
}


</style>